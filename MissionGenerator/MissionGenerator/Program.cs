﻿using System;
using System.Collections.Generic;

class MainClass {

	public static string mainMenu()
	{

        Console.WriteLine("Choose mission to add(enter q to leave):");
		Console.WriteLine("1 : killTask");

		return Console.ReadLine();
	}

    public static void Main(string[] args) {
        MissionGenerator mg;
        Console.WriteLine("[Mission Generator]");
        Console.WriteLine("Enter missionName:");
        string name;
        name = Console.ReadLine();

        while (true) {
            switch (mainMenu())
            {
                case "1":
                    mg = new MissionGenerator(createKillTask(name));
                    break;
                case "q":
                    return;
                default:
                    Console.WriteLine("Q__________Q");
                    break;
            }
        }
    }

    public static KillTask createKillTask (string missionName) {
        string name;
        Dictionary<string, int> dic = new Dictionary<string, int>();
        Console.WriteLine("Enter mobs(enter 'q' to quit)");
        while (true) {
            Console.Write("Enter mob Type:");
            name = Console.ReadLine();
            if (name == "q")
                break;
            Console.Write("Enter mob massacare size:");
            dic.Add(name, Convert.ToInt32(Console.ReadLine()));
        }
        return new KillTask(missionName, dic);
    }
}
