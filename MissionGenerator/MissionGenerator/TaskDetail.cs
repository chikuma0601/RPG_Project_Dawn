﻿using System;


public class TaskDetail {
	string name;
	string quantity;

	public TaskDetail (string name, string quantity) {
		this.name = name;
		this.quantity = quantity;
	}

	public string getValue {
		get {
			return quantity;
		}
	}
}
