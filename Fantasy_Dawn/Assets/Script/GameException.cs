﻿using System;


public class GameException : Exception {
	public GameException (string message) :base(message) {
	}
}

public class UnmatchException : GameException {
	public UnmatchException (string msg) :base(msg) {
	}
}

public class TargetNotFoundException : GameException {
	public TargetNotFoundException (string msg) :base(msg) {
	}
}

public class ExceedLimitException : GameException {
	public ExceedLimitException (string msg) :base(msg) {
	}
}

public class CrappyException : GameException {
	public CrappyException (string msg) : base(msg) {
	}
}

public class RequireNotMatchException : GameException {
	public RequireNotMatchException (string msg) :base(msg) {
	}
}