﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Backpack {
	private int backpackLimit = 50;
	private List<Item> pack = new List<Item> ();

	public void addItem (Item i) {
		if (pack.Count < backpackLimit) {
			foreach (Item item in pack)
				if (i.getItemCode () == item.getItemCode ()) {
					item.setQuantity (item.getQuantity () + 1);
					return;
				}
			pack.Add (i);
		}
		else
			GameObject.Find ("Canvas").transform.Find ("hind").GetComponent<GUIHint> ().showHintWindow ("背包： 我的肚子塞滿了ＯＯ");
	}

	public void addItem (List<Item> items) {
		if (items != null) {
			if ((pack.Count + items.Count) < backpackLimit)
				foreach (Item item in items)
					addItem (item);
			else
				GameObject.Find ("Canvas").transform.Find ("hind").GetComponent<GUIHint> ().showHintWindow ("背包： 我的肚子塞滿了ＯＯ");
				//throw new ExceedLimitException ("Backpack exceeded its limit with maximum = " + backpackLimit);
		}
	}
		
	public Item getItem (int i) {
		return pack [i];
	}

	public void removeItem (int i) {
		pack.RemoveAt(i);
	}

	private int idComparison (Item a, Item b) {	// for sortById()
		if (a.getItemCode () > b.getItemCode ())
			return 1;
		else if (a.getItemCode () < b.getItemCode ())
			return -1;
		else
			return 0;
	}

	public void sortbyId() {
		pack.Sort (idComparison);
		stackItem ();
	}

	public override string ToString () {
		string inv = "";
		foreach (Item item in pack)
			inv += item.toString() + "\n";
		return inv;
	}

	public void stackItem () {
		for(int i = 0; i < pack.Count; ++i ) {
			if (!(pack [i] is Weapon)) {
				int quantity = pack [i].getQuantity ();
				for (int j = i + 1; j < pack.Count;) {
					if (pack [j].getItemCode () == pack [i].getItemCode ()) {
						quantity += pack [j].getQuantity ();
						pack.RemoveAt (j);
					} else
						break;
				}
				pack [i].setQuantity (quantity);
			}
		}
	}

	public void use (int index) {
		if (!pack [index].use ())
			pack.RemoveAt (index);
	}

	public void showInventory () {
		MonoBehaviour.print ("inventory:\n" + ToString ());
	}

	public int getSize () {
		return pack.Count;
	}
}
