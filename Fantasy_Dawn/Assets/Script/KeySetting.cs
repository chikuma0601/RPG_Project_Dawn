﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class KeySetting {
	public Dictionary<string, KeyCode> move = new Dictionary<string, KeyCode> () {
		{ "up", KeyCode.None },
		{ "down", KeyCode.None },
		{ "left", KeyCode.None },
		{ "right", KeyCode.None }
	};
	public Dictionary<string, KeyCode> skill = new Dictionary<string, KeyCode> ();
	public Dictionary<string, KeyCode> shortCut = new Dictionary<string, KeyCode> ();

	public KeySetting () {}
	public KeySetting(KeySetting old){
		this.move = new Dictionary<string, KeyCode> (old.move);
		this.skill = new Dictionary<string, KeyCode> (old.skill);
		this.shortCut = new Dictionary<string, KeyCode> (old.shortCut);
	}

	public void moveSet (string direction, KeyCode key) {
		move [direction] = key;
	}
}
