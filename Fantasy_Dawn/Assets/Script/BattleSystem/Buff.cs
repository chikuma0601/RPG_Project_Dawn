﻿using System;
using UnityEngine;

public abstract class Buff {
	public float buffDuration;
	public string name;

	public abstract string toString ();
}

public class Atk : Buff {
	public int effect;

	public Atk (float duration, int effectValue) {
		buffDuration = duration;
		effect = effectValue;
		name = "Atk";
	}

	public override string ToString () {
		return "buff: Atk, effect: " + effect + "duration: " + buffDuration;
	}

	public override string toString () {
		return ToString ();
	}
}

public class Def : Buff {
	public int effect;
	public Def (float duration, int effectValue) {
		buffDuration = duration;
		effect = effectValue;
		name = "Def";
	}

	public override string ToString () {
		return "buff: Def, effect: " + effect + "duration: " + buffDuration;
	}

	public override string toString () {
		return ToString ();
	}
}

public class Poison : Buff {
	public float effect;
	public Poison (float duration, float effectValue) {
		buffDuration = duration;
		effect = effectValue;
		name = "Poison";
	}

	public override string ToString () {
		return "buff: Poison\neffect: " + effect + "\nduration: " + buffDuration;
	}

	public override string toString () {
		return ToString ();
	}
}

public class Stun : Buff {
	public Stun (float duration) {
		buffDuration = duration;
		name = "Stun";
	}
	public override string ToString () {
		return "buff: Stun, duration: " + buffDuration;
	}

	public override string toString () {
		return ToString ();
	}
}

public class Slow : Buff {
	public readonly float effect = 0.5f;
	public Slow (float duration) {
		buffDuration = duration;
		name = "Slow";
	}

	public override string ToString () {
		return "buff: Slow, duration: " + buffDuration;
	}

	public override string toString () {
		return ToString ();
	}
}

public class Recover : Buff {
	public float effect;
	public Recover (float healingValue) {
		buffDuration = 0;
		effect = healingValue;
		name = "Recover";
	}

	public override string ToString () {
		return "buff: Recover, effect: " + effect;
	}

	public override string toString () {
		return ToString ();
	}
}

public class KnockBack : Buff {
	public float distance;
	public GameObject attacker;

	public KnockBack (float d, GameObject a) {
		buffDuration = 0;
		distance = d;
		attacker = a;
		name = "KnockBack";
	}

	public override string ToString () {
		return "buff: KnockBack, effect: " + distance;
	}

	public override string toString () {
		return ToString ();
	}

	public GameObject getAttacker () {
		return attacker;
	}
}