﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Skill {
	public string name;
	public float skillAttackRate;
	public List<Buff> buffHostile = new List<Buff> ();
	public List<Buff> buffSelf = new List<Buff> ();
	public float cd;
}

// poison attack
public class AttackWithPoison : Skill {
	public AttackWithPoison (float duration, float poisonDamage, float coolDown) {
		skillAttackRate = 0.8f;
		name = "poisonAttack";
		cd = coolDown;
		buffHostile.Add (new Poison (duration, poisonDamage));
	}
}

public class NormalAttack : Skill {
	public NormalAttack (float coolDown, GameObject attacker) {
		name = "normalAttack";
		cd = coolDown;
		skillAttackRate = 1f;
		buffHostile.Add (new KnockBack (5, attacker));
	}
}

public class HeavyAttack : Skill {
	public HeavyAttack(float duration, float cooldown) {
		skillAttackRate = 1.5f;
		name = "HeavyAttack";
		cd = cooldown;
		buffHostile.Add (new Slow (duration));
	}
}
