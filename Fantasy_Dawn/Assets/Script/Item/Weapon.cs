﻿using System;

public class Weapon : Item {
	private int damage;

	public Weapon (int itemCode, string name, int damage, string description, int price) {
		this.itemCode = itemCode;
		this.name = name;
		this.damage = damage;
		this.description = description;
		this.price = price;
	}

	public Weapon (Weapon origin) {
		this.itemCode = origin.itemCode;
		this.name = origin.name;
		this.damage = origin.damage;
		this.description = origin.description;
	}

	public override Item clone () {
		return new Weapon (this);
	}

	public int getDamage () {
		return damage;
	}

	public override string toString () {
		return "weadpon : " + name + ", damage : " + damage;
	}

	public override string getDescription () {
		return "description : " + description;
	}

	public override string ToString () {
		return toString ();
	}

	public override bool use () {
		UnityEngine.GameObject.Find ("player").GetComponent<PlayerController> ().equip (new Weapon(this));
		UnityEngine.MonoBehaviour.print ("Weapon is used");
		return true;
	}
}
