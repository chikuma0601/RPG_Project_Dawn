﻿using System;
using System.Collections.Generic;
using System.Xml;

public class ItemReader {
	private XmlDocument itemFile = new XmlDocument ();
	private XmlNode item;
	private string filePath = "../../../Assets/Resources/item.xml";		//for ut

	public ItemReader () {}
	public ItemReader (string filePath) {
		this.filePath = filePath;
	}

	public Item read (string code) {
		try {
			itemFile.LoadXml (filePath);
		}
		catch (XmlException e) {
			itemFile.Load (filePath);
		}

		item = itemFile.SelectSingleNode ("item/itemCode[attribute::code=" + code + "]");
		if (item == null)
			throw new UnmatchException("unmatch item code");
		return createItem (Convert.ToInt32(code));
	}

	public Item createItem (int code) {
		string name = item.SelectSingleNode ("name").InnerText;
		string description = item.SelectSingleNode ("description").InnerText;
		int price = Convert.ToInt32 (item.SelectSingleNode ("price").InnerText);

		switch (item.Attributes["type"].Value) {
		case "weapon":
			int damage = Convert.ToInt32 (item.SelectSingleNode ("damage").Attributes ["dmg"].Value);
			return new Weapon (code, name, damage, description, price);
		case "potion":
			return new Potion (code, name, createPotion(), description, price);
		default:
			throw new UnmatchException("unknown itemType: " + item.SelectSingleNode("itemType").ToString());
		}
	}

	public List<Buff> createPotion () {
		List<Buff> effects = new List<Buff> ();
		foreach (XmlNode xn in item.SelectSingleNode("effect").ChildNodes){
			switch (xn.Name) {
			case "recover":
				effects.Add (new Recover (Convert.ToInt32 (xn.Attributes ["pt"].Value)));
				break;
			default:
				throw new UnmatchException ("Unknown effect :( name: " + xn.Name);
			}
		}
		return new List<Buff> (effects);
	}
}


