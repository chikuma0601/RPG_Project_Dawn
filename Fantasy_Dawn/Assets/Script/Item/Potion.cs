﻿using System;
using System.Collections.Generic;


public class Potion : Item {
	private List<Buff> effects = new List<Buff> ();
	private string effectDetail;

	public Potion (int itemCode, string name, List<Buff> buffs, string description, int price) {
		this.itemCode = itemCode;
		this.name = name;
		effects.AddRange (buffs);
		this.description = description;
		foreach (Buff buff in effects)
			effectDetail += buff.toString () + "\n";
		this.price = price;
	}

	public Potion (int itemCode, string name, List<Buff> buffs, string description, int price, int quantity) {
		this.itemCode = itemCode;
		this.name = name;
		effects.AddRange (buffs);
		this.quantity = quantity;
		this.description = description;
		this.price = price;
	}

	public Potion (Potion p) {
		itemCode = p.itemCode;
		name = p.name;
		effects = new List<Buff> (p.effects);
		quantity = p.quantity;
		description = p.description;
		price = p.price;
	}

	public override Item clone () {
		return new Potion (this);
	}

	public string getEffectDescription () {
		return effectDetail;
	}

	public override string toString () {
		return "potion :" + name + ", quantity : " + quantity;
	}

	public override string getDescription () {
		return "description : " + description + "\n" + getEffectDescription();
	}

	public override string ToString () {
		return toString ();
	}

	public override bool use () {
		Creature player = UnityEngine.GameObject.Find ("player").GetComponent<Creature> ();
		foreach (Buff buff in effects)
			player.gainBuff (buff);
		quantity--;
		return quantity != 0;
	}
}

