﻿using System;

public class Armor : Item {
	private int defend;

	public Armor (int itemCode, string name, int defend, string description, int price) {
		this.itemCode = itemCode;
		this.name = name;
		this.defend = defend;
		this.description = description;
		this.price = price;
	}

	public Armor (Armor a) {
		itemCode = a.itemCode;
		name = a.name;
		defend = a.defend;
		description = a.description;
		price = a.price;
	}

	public override Item clone () {
		return new Armor (this);
	}

	public int getDefend () {
		return defend;
	}

	public override string toString () {
		return "armor : " + name + ", defend : " + defend;
	}

	public override string getDescription () {
		return "description : " + description;
	}

	public override string ToString () {
		return toString ();
	}

	public override bool use () {
		UnityEngine.GameObject.Find ("player").GetComponent<PlayerController> ().equip (this);
		UnityEngine.MonoBehaviour.print ("Armor is used");
		return true;
	}
}
