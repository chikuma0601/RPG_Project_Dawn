﻿using System;
using System.Collections.Generic;

public abstract class Item {	//interface
	protected int itemCode;		//Item code is used to present the kind of the item.
	protected int price;
	protected int quantity = 1;
	protected string name;
	protected string description;

	public string getName () {
		return name;
	}

	public int getQuantity () {
		return quantity;
	}

	public int getItemCode () {
		return itemCode;
	}

	public string getDescrition () {
		return description;
	}

	public void setQuantity (int i) {
		quantity = i;
	}

	public int getPrice () {
		return price;
	}

	public abstract Item clone ();
	public abstract string toString ();
	public abstract string getDescription ();
	public abstract bool use ();

}

