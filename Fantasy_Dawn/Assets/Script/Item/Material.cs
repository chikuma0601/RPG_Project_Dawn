﻿using System;

public class Material : Item {

	public Material (int itemCode, string name, string description, int price) {
		this.itemCode = itemCode;
		this.name = name;
		this.description = description;
		this.price = price;
	}

	public Material (Material m) {
		itemCode = m.itemCode;
		name = m.name;
		description = m.description;
		price = m.price;
	}

	public override Item clone () {
		return new Material (this);
	}

	public override string toString () {
		return "Material : " + name;
	}

	public override string getDescription () {
		return "說明 : " + description;
	}

	public override string ToString () {
		return toString ();
	}

	public override bool use () {
		UnityEngine.MonoBehaviour.print ("Item can't be used");
		return true;
	}

}
