﻿using System;
using System.Collections.Generic;
using UnityEngine;


public class GUIMission : GUIWindow {
	private BoxSetting missionListBox;
	private BoxSetting scrollBox;
	private BoxSetting missionDetailBox;
	private BoxSetting missionBox;

	private List<Mission> missionList;
	private Mission selectedMission = null;

	private string title = "任務列表";
	private Texture2D transparentBackground;
	private Font font;

	private float missionInView;
	private bool showMissionWindow = false;

	// scroll setting
	private float barSize;
	private float scrollDelta = 0;
	private float scrollSpeed = 0.5f;



	void Start () {
		missionList = GameObject.Find ("gameManager").GetComponent<DataLib> ().getMissionList ();

		window.height = Screen.height * 0.8f;
		window.width = window.height / 1.414f;
		window.position = new Vector2 (Screen.width - window.width - 10, 10);

		label.position = Vector2.zero;
		label.width = window.width;
		label.height = 30;

		missionListBox.position = new Vector2 (10, label.height + 10);
		missionListBox.height = window.height - label.height - 20;
		missionListBox.width = (window.width - 20) / 2 - 20;

		scrollBox.position = missionListBox.position + new Vector2 (missionListBox.width + 10, 0);
		scrollBox.height = missionListBox.height;
		scrollBox.width = 20;

		missionDetailBox.position = scrollBox.position + new Vector2 (scrollBox.width + 10, 0);
		missionDetailBox.width = window.width - missionDetailBox.position.x - 10;
		missionDetailBox.height = missionListBox.height;

		missionBox.height = 30;
		missionBox.width = missionListBox.width - 10;

		missionInView = missionListBox.height / missionBox.height;

		font = Resources.Load ("Font/NotoSansHant-Light") as Font;
		transparentBackground = Resources.Load ("empty") as Texture2D;
	}

	void OnGUI () {
		if (showMissionWindow) {
			// window Area
			GUILayout.BeginArea (window.toRect());
				setBackground ();
				setLabel ();
				setMissionList ();
				setScroll ();
				setMissionDetail ();
			GUILayout.EndArea ();
		}
	}
		
	private void setBackground () {
		GUI.DrawTexture (new Rect (0, label.height, window.width, window.height - label.height), Resources.Load ("BackpackGUI/background") as Texture);
	}

	private void setLabel () {
		GUIStyle labelStyle = new GUIStyle(GUI.skin.label);
		labelStyle.font = font;
		labelStyle.fontSize = 18;
		labelStyle.normal.textColor = Color.black;

		GUI.DrawTexture (label.toRect (), Resources.Load ("BackpackGUI/titleBar") as Texture);

		// place text in the middle of label
		GUILayout.BeginArea (label.toRect());
		GUILayout.BeginHorizontal ();
		GUILayout.FlexibleSpace ();
		GUILayout.BeginVertical ();
		GUILayout.FlexibleSpace ();
		GUILayout.Label (title, labelStyle);
		GUILayout.FlexibleSpace ();
		GUILayout.EndVertical ();
		GUILayout.FlexibleSpace ();
		GUILayout.EndHorizontal ();
		GUILayout.EndArea ();

		setExitButton ();
	}

	private void setExitButton () {
		GUILayout.BeginHorizontal ();
		GUILayout.FlexibleSpace ();
		if (GUILayout.Button ("X", GUILayout.MaxHeight (label.height), GUILayout.MaxWidth (label.height))) 
			showMissionWindow = false;
		GUILayout.EndHorizontal ();
	}

	private void setScroll () {
		int count = missionList.Count;
		if (count > missionInView) {
			barSize = missionInView / count;
		} else {
			GUI.VerticalScrollbar (scrollBox.toRect (), 0, 1, 0, 1);
			scrollDelta = 0;
			barSize = 0;
		}
		scrollDelta = GUI.VerticalScrollbar (scrollBox.toRect (), scrollDelta, barSize, 0, 1);
	}

	private void setMissionList () {
		float originYPosition = - (missionList.Count * missionBox.height - missionListBox.height) * (scrollDelta / (1 - barSize)) + 5;

		GUIStyle buttonStyle = new GUIStyle (GUI.skin.button);
		buttonStyle.normal.background = transparentBackground;
		buttonStyle.alignment = TextAnchor.MiddleCenter;

		GUIStyle boxStyle = new GUIStyle (GUI.skin.box);
		boxStyle.alignment = TextAnchor.MiddleLeft;

		GUI.Box (missionListBox.toRect (), "");
		GUILayout.BeginArea (missionListBox.toRect ());
			int i = 0;

			// mission not complete
			GUI.Box (new Rect (5, originYPosition + i * (missionBox.height + 5), missionBox.width, missionBox.height), "進行中任務", boxStyle);
			i++;
			foreach (Mission mission in missionList)
				if (!mission.isComplete ()) {
					GUI.Box (new Rect (5, originYPosition + i * (missionBox.height + 5), missionBox.width, missionBox.height), mission.getMissionName ());
					if (GUI.Button (new Rect (5, originYPosition + i * (missionBox.height + 5), missionBox.width, missionBox.height), "", buttonStyle))
						selectedMission = mission;
					i++;
				}

			// mission complete
			GUI.Box (new Rect (5, originYPosition + i * (missionBox.height + 5), missionBox.width, missionBox.height), "已完成任務", boxStyle);
			i++;
			foreach (Mission mission in missionList)
				if (mission.isComplete ()) {
					GUI.Box (new Rect (5, originYPosition + i * (missionBox.height + 5), missionBox.width, missionBox.height), mission.getMissionName ());
					if (GUI.Button (new Rect (5, originYPosition + i * (missionBox.height + 5), missionBox.width, missionBox.height), "", buttonStyle))
						selectedMission = mission;
					i++;
				}
		GUILayout.EndArea();
	}

	private void setMissionDetail () {
		GUI.Box (missionDetailBox.toRect (), "");
		if (selectedMission != null)
			GUI.Box (missionDetailBox.toRect (), selectedMission.toString ());
	}

	public void scrollByMouseWheel (float value) {
		if (mouseOnWindow ())
			scrollDelta -= value * scrollSpeed;
	}

	public void openMissionWindow () {
		showMissionWindow = !showMissionWindow;
		scrollDelta = 0;
	}
}


