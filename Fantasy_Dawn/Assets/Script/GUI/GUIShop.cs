﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIShop : GUIWindow {
	private List<Item> commodity;
	private Backpack inventory;
	private string title = "商店";
	private Font font;
	private bool showShop = false;
	private float scrollSpeed = .5f;
	private int selectedCommodityIndex;
	private int selectedInventoryItemIndex;

	//public GameObject announcement;

	// window size
	private BoxSetting itemView;
	private BoxSetting scroll;
	private BoxSetting itemBox;
	private BoxSetting itemOption;

	private float iconSize = 50;

	// scroll setting
	private float scrollDelta = 0;
	private float barSize;

	private float itemInView;

	private bool isCommodityClicked = false;
	private bool isBuy = true;

	Texture2D itemBoxBackground;

	public void openShopWindow (List <Item> commodity) {
		if (commodity == null) {
			showShop = false;
			return;
		}
		this.commodity = new List<Item> (commodity);
		inventory = GameObject.Find ("gameManager").GetComponent<DataLib> ().inventory;
		showShop = !showShop;
	}

	public void buy () {
		if (commodity [selectedCommodityIndex].getPrice () <= GameObject.Find ("gameManager").GetComponent<DataLib> ().getMoney ()) {
			inventory.addItem (commodity [selectedCommodityIndex].clone());
			GameObject.Find ("gameManager").GetComponent<DataLib> ().addMoney (-commodity [selectedCommodityIndex].getPrice ());
		} else {
			GameObject.Find ("Canvas").transform.Find ("hint").GetComponent<GUIHint> ().showHintWindow("挖某及");
		}
	}

	public void sell () {
		GameObject.Find ("gameManager").GetComponent<DataLib> ()
			.addMoney (inventory.getItem(selectedInventoryItemIndex).getPrice () / 2 * inventory.getItem(selectedInventoryItemIndex).getQuantity());
		inventory.removeItem (selectedInventoryItemIndex);
	}

	void Start () {
		window.height = Screen.height * 0.8f;
		window.width = window.height / 1.414f;
		window.position = new Vector2 (Screen.width - window.width - 10, 10);

		label.position = Vector2.zero;
		label.width = window.width;
		label.height = 30;

		itemBox.width = window.width - scroll.width - 30;							// left boundary: 30
		itemBox.height = 60;

		itemView.position = new Vector2 (0, label.height + 10);						// up boundary: 10
		itemView.width = itemBox.width;
		itemView.height = window.height - label.height - 80;							// up and down boundary: 80

		scroll.position = new Vector2 (window.width - 22.5f, label.height + 10); 	// up boundary: 10
		scroll.width = 20;
		scroll.height = window.height - label.height - 80;							// up and down boundary: 80

		itemOption.height = 200;
		itemOption.width = 200;

		font = Resources.Load ("Font/NotoSansHant-Light") as Font;
		itemInView = itemView.height / itemBox.height;

		itemBoxBackground = Resources.Load ("empty") as Texture2D;

		//announcement.SetActive (false);
	}

	void OnGUI () {
		if (showShop) {
			// window Area
			GUILayout.BeginArea (window.toRect());
				setBackground ();
				setLabel ();
				setScroll ();
				if (isBuy)
					commodityToGUI ();
				else
					backpackToGUI ();
				createBuyOrSellButton ();
			GUILayout.EndArea ();
			if (isCommodityClicked)
				itemOnClick ();
		}
	}

	private void setScroll () {
		int count = isBuy ? commodity.Count : inventory.getSize ();
		if (count > itemInView) {
			barSize = itemInView / count;
		} else {
			GUI.VerticalScrollbar (scroll.toRect (), 0, 1, 0, 1);
			scrollDelta = 0;
			barSize = 0;
		}
		scrollDelta = GUI.VerticalScrollbar (scroll.toRect (), scrollDelta, barSize, 0, 1);
	}

	private void setBackground () {
		GUI.DrawTexture (new Rect (0, label.height, window.width, window.height - label.height), Resources.Load ("BackpackGUI/background") as Texture);
	}

	private void setLabel () {
		GUIStyle labelStyle = new GUIStyle(GUI.skin.label);
		labelStyle.font = font;
		labelStyle.fontSize = 18;
		labelStyle.normal.textColor = Color.black;

		GUI.DrawTexture (label.toRect (), Resources.Load ("BackpackGUI/titleBar") as Texture);

		// place text in the middle of label
		GUILayout.BeginArea (label.toRect());
		GUILayout.BeginHorizontal ();
		GUILayout.FlexibleSpace ();
		GUILayout.BeginVertical ();
		GUILayout.FlexibleSpace ();
		GUILayout.Label (title, labelStyle);
		GUILayout.FlexibleSpace ();
		GUILayout.EndVertical ();
		GUILayout.FlexibleSpace ();
		GUILayout.EndHorizontal ();
		GUILayout.EndArea ();

		setExitButton ();
	}

	private void setExitButton () {
		GUILayout.BeginHorizontal ();
		GUILayout.FlexibleSpace ();
		if (GUILayout.Button ("X", GUILayout.MaxHeight (label.height), GUILayout.MaxWidth (label.height))) 
			showShop = false;
		GUILayout.EndHorizontal ();
	}

	private void backpackToGUI () {
		// itemView area
		GUILayout.BeginArea (itemView.toRect ());
		float originYPosition = - (inventory.getSize() * itemBox.height - itemView.height) * (scrollDelta / (1 - barSize));
		string description = "";

		for (int i = 0 ; i < inventory.getSize() ; ++i){
			switch (inventory.getItem(i).GetType ().Name) {
			case "Weapon":
				Weapon weapon = inventory.getItem(i) as Weapon;
				description = weapon.getName () + "\n 攻擊力: " + weapon.getDamage ();
				createItemBox (originYPosition + i * itemBox.height, i, description);
				break;
			case "Armor":
				Armor armor = inventory.getItem(i) as Armor;
				description = armor.getName () + "\n 防禦力: " + armor.getDefend ();
				createItemBox (originYPosition + i * itemBox.height, i, description);
				break;
			case "Potion":
				Potion potion = inventory.getItem(i) as Potion;
				description = potion.getName () + "\n 數量: " + potion.getQuantity ();
				createItemBox (originYPosition + i * itemBox.height, i, description);
				break;
			case "Material":
				Material material = inventory.getItem(i) as Material;
				description = material.getName () + "\n 數量: " + material.getQuantity ();
				createItemBox (originYPosition + i * itemBox.height, i, description);
				break;
			default:
				throw new UnmatchException ("WTF is this item? name:" + inventory.getItem(i).GetType ().Name);
			}
		}
		GUILayout.EndArea ();
	}

	private void commodityToGUI () {
		// itemView area
		GUILayout.BeginArea (itemView.toRect ());
		float originYPosition = - (commodity.Count * itemBox.height - itemView.height) * (scrollDelta / (1 - barSize));
		string description = "";

		for (int i = 0 ; i < commodity.Count ; ++i){
			switch (commodity[i].GetType ().Name) {
			case "Weapon":
				Weapon weapon = commodity[i] as Weapon;
				description = weapon.getName () + "\n 攻擊力: " + weapon.getDamage ();
				createItemBox (originYPosition + i * itemBox.height, i, description);
				break;
			case "Armor":
				Armor armor = commodity[i] as Armor;
				description = armor.getName () + "\n 防禦力: " + armor.getDefend ();
				createItemBox (originYPosition + i * itemBox.height, i, description);
				break;
			case "Potion":
				Potion potion = commodity[i] as Potion;
				description = potion.getName () + "\n 數量: " + potion.getQuantity ();
				createItemBox (originYPosition + i * itemBox.height, i, description);
				break;
			case "Material":
				Material material = commodity[i] as Material;
				description = material.getName () + "\n 數量: " + material.getQuantity ();
				createItemBox (originYPosition + i * itemBox.height, i, description);
				break;
			default:
				throw new UnmatchException ("WTF is this item? name:" + commodity[i].GetType ().Name);
			}
		}
		GUILayout.EndArea ();
	}

	private void createItemBox (float positionY, int itemIndex, string description) {
		GUILayout.BeginArea (new Rect (5, positionY, itemBox.width, itemBox.height));							// left boundary: 5
		createInvisibleButton (itemIndex);
		Sprite icon = Resources.Load <Sprite> ("temp2");
		showIcon (icon);
		Rect BoxRect = new Rect (iconSize + 20, 5,itemBox.width - iconSize - 30, iconSize);					// left and right boundary: 30
		GUI.Box (BoxRect, description, itemBoxStyleSetting ());
		GUILayout.EndArea ();
	}

	private void createInvisibleButton (int itemIndex) {
		GUIStyle buttonStyle = new GUIStyle (GUI.skin.button);
		buttonStyle.normal.background = itemBoxBackground;
		//buttonStyle.hover.background = makeTexture ((int)itemBox.width, (int)itemBox.height, new Color(0, 0, 0, 0));
		//buttonStyle.active.background = makeTexture ((int)itemBox.width, (int)itemBox.height, new Color(0, 0, 0, 0)); //<-----CRAPPY JUNK
		if (GUI.Button (new Rect (0, 0, itemBox.width, itemBox.height), "", buttonStyle)) {
			if (isBuy)
				selectedCommodityIndex = itemIndex;
			else
				selectedInventoryItemIndex = itemIndex;
			isCommodityClicked = true;
			frameDelay = 0;
		}
	}

	private int frameDelay = 0;
	private void itemOnClick () {			
		if (frameDelay < 2) {
			frameDelay ++;
			// itemOption.position = screenToGUICoordinate ((Vector2)Input.mousePosition);			// ideal situation
			itemOption.position = window.position;
			itemOption.position.x -= itemOption.width;
		}

		GUILayout.BeginArea (itemOption.toRect ());
			GUI.Box (new Rect (0, 0, itemOption.width, itemOption.height), "");
			GUIStyle descriptionStyle = new GUIStyle(GUI.skin.box);
			descriptionStyle.alignment = TextAnchor.UpperLeft;
			string description;
			if(isBuy)
				description = commodity[selectedCommodityIndex].getDescription () + "\n價格: " + commodity[selectedCommodityIndex].getPrice ();
			else	
				description = inventory.getItem(selectedInventoryItemIndex).getDescription () + "\n價格: " + inventory.getItem(selectedInventoryItemIndex).getPrice () / 2;
			GUI.Box (new Rect (5, 5, itemOption.width - 10, itemOption.height - 40), description, descriptionStyle);
			createOptionButton ();
		GUILayout.EndArea ();
	}

	private void createOptionButton () {
		if (isBuy) {
			if (GUI.Button (new Rect (5, itemOption.height - 30, 40, 20), "購買")) {
				buy ();
				isCommodityClicked = false;
			}
			if (GUI.Button (new Rect (50, itemOption.height - 30, 40, 20), "返回")) {
				isCommodityClicked = false;
			}
		} else {
			if (GUI.Button (new Rect (5, itemOption.height - 30, 40, 20), "賣出")) {
				sell ();
				isCommodityClicked = false;
			}
			if (GUI.Button (new Rect (50, itemOption.height - 30, 40, 20), "返回")) {
				isCommodityClicked = false;
			}
		}
	}

	public GUIStyle itemBoxStyleSetting () {
		GUIStyle boxStyle = new GUIStyle(GUI.skin.box);
		boxStyle.alignment = TextAnchor.MiddleLeft;
		boxStyle.font = font;
		boxStyle.fontSize = 12;
		boxStyle.normal.textColor = Color.black;
		return new GUIStyle (boxStyle);
	}

	private void showIcon (Sprite icon) {
		GUI.DrawTexture (new Rect(10, 5, iconSize, iconSize), icon.texture); 										// left boundary: 10, up boundary: 5
	}

	public void scrollByMouseWheel (float value) {
		if (mouseOnWindow())
			scrollDelta -= value * scrollSpeed;
	}

	private void createBuyOrSellButton () {
		if (GUI.Button (new Rect (15, label.height + itemView.height + 20, 50, 50), buyChange())) {
			isBuy = !isBuy; 
		}
			// change to sell or buy
	}

	private string buyChange () {
		if (isBuy)
			return "賣出";
		else
			return "買入";
	}
}
