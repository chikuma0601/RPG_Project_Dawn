﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIInventory : GUIWindow {
	private Backpack pack;
	private int selectedItemIndex;
	private string title = "背包";
	private Font font;
	private bool showBackpack = false;
	private float scrollSpeed = .5f;

	// window size
	private BoxSetting itemView;
	private BoxSetting scroll;
	private BoxSetting itemBox;
	private BoxSetting itemOption;

	private float iconSize = 50;

	// scroll setting
	private float scrollDelta = 0;
	private float barSize;

	private float itemInView;

	private bool isItemClicked = false;

	Texture2D itemBoxBackground;

	void Start () {
		window.height = Screen.height * 0.8f;
		window.width = window.height / 1.414f;
		//window.position = new Vector2 (Screen.width / 2 - window.width / 2, Screen.height / 2 - window.height / 2);
		window.position = new Vector2(10, 10);

		label.position = Vector2.zero;
		label.width = window.width;
		label.height = 30;

		itemBox.width = window.width - scroll.width - 30;							// left boundary: 30
		itemBox.height = 60;

		itemView.position = new Vector2 (0, label.height + 10);						// up boundary: 10
		itemView.width = itemBox.width;
		itemView.height = window.height - label.height - 80;							// up and down boundary: 80

		scroll.position = new Vector2 (window.width - 22.5f, label.height + 10); 	// up boundary: 10
		scroll.width = 20;
		scroll.height = window.height - label.height - 80;							// up and down boundary: 80

		itemOption.height = 200;
		itemOption.width = 200;

		font = Resources.Load ("Font/NotoSansHant-Light") as Font;
		itemInView = itemView.height / itemBox.height;

		itemBoxBackground = Resources.Load ("empty") as Texture2D;
	}

	void OnGUI () {
		if (showBackpack) {
			// window Area
			GUILayout.BeginArea (window.toRect());
				setBackground ();
				setLabel ();
				setScroll ();
				itemToGUI ();
				createSortButon ();
				createMoneyBar ();
			GUILayout.EndArea ();
			if (isItemClicked)
				itemOnClick ();
		}
	}

	private void setScroll () {
		if (pack.getSize () > itemInView) {
			barSize = itemInView / pack.getSize ();
			scrollDelta = GUI.VerticalScrollbar (scroll.toRect (), scrollDelta, barSize, 0, 1);

		
		} else {
			GUI.VerticalScrollbar (scroll.toRect (), 0, 1, 0, 1);
			scrollDelta = 0;
			barSize = 0;
		}
	}

	private void setBackground () {
		GUI.DrawTexture (new Rect (0, label.height, window.width, window.height - label.height), Resources.Load ("BackpackGUI/background") as Texture);
	}

	private void setLabel () {
		GUIStyle labelStyle = new GUIStyle(GUI.skin.label);
		labelStyle.font = font;
		labelStyle.fontSize = 18;
		labelStyle.normal.textColor = Color.black;

		GUI.DrawTexture (label.toRect (), Resources.Load ("BackpackGUI/titleBar") as Texture);

		// place text in the middle of label
		GUILayout.BeginArea (label.toRect());
		GUILayout.BeginHorizontal ();
		GUILayout.FlexibleSpace ();
		GUILayout.BeginVertical ();
		GUILayout.FlexibleSpace ();
			GUILayout.Label (title, labelStyle);
		GUILayout.FlexibleSpace ();
		GUILayout.EndVertical ();
		GUILayout.FlexibleSpace ();
		GUILayout.EndHorizontal ();
		GUILayout.EndArea ();
			
		setExitButton ();
	}

	private void setExitButton () {
		GUILayout.BeginHorizontal ();
		GUILayout.FlexibleSpace ();
		if (GUILayout.Button ("X", GUILayout.MaxHeight (label.height), GUILayout.MaxWidth (label.height)))
			showBackpack = false;
		GUILayout.EndHorizontal ();
	}

	public void openInventory () {
		pack = GameObject.Find ("gameManager").GetComponent<DataLib> ().inventory;
		isItemClicked = false;
		showBackpack = !showBackpack;
	}

	private void itemToGUI () {
		// itemView area
		GUILayout.BeginArea (itemView.toRect ());
		float originYPosition = - (pack.getSize () * itemBox.height - itemView.height) * (scrollDelta / (1 - barSize));
		string description = "";

		for (int i = 0 ; i < pack.getSize() ; ++i){
			switch (pack.getItem (i).GetType ().Name) {
			case "Weapon":
				Weapon weapon = pack.getItem (i) as Weapon;
				description = weapon.getName () + "\n 攻擊力: " + weapon.getDamage ();
				createItemBox (originYPosition + i * itemBox.height, i, description);
				break;
			case "Armor":
				Armor armor = pack.getItem (i) as Armor;
				description = armor.getName () + "\n 防禦力: " + armor.getDefend ();
				createItemBox (originYPosition + i * itemBox.height, i, description);
				break;
			case "Potion":
				Potion potion = pack.getItem (i) as Potion;
				description = potion.getName () + "\n 數量: " + potion.getQuantity ();
				createItemBox (originYPosition + i * itemBox.height, i, description);
				break;
			case "Material":
				Material material = pack.getItem (i) as Material;
				description = material.getName () + "\n 數量: " + material.getQuantity ();
				createItemBox (originYPosition + i * itemBox.height, i, description);
				break;
			default:
				throw new UnmatchException ("WTF is this item? name:" + pack.getItem (i).GetType ().Name);
			}
		}
		GUILayout.EndArea ();
	}
		
	private void createItemBox (float positionY, int itemIndex, string description) {
		GUILayout.BeginArea (new Rect (5, positionY, itemBox.width, itemBox.height));							// left boundary: 5
		createInvisibleButton (itemIndex);
			Sprite icon = Resources.Load <Sprite> ("temp2");
			showIcon (icon);
			Rect BoxRect = new Rect (iconSize + 20, 5,itemBox.width - iconSize - 30, iconSize);					// left and right boundary: 30
		GUI.Box (BoxRect, description, itemBoxStyleSetting ());
		GUILayout.EndArea ();
	}

	private void createInvisibleButton (int itemIndex) {
		GUIStyle buttonStyle = new GUIStyle (GUI.skin.button);
		buttonStyle.normal.background = itemBoxBackground;
		//buttonStyle.hover.background = makeTexture ((int)itemBox.width, (int)itemBox.height, new Color(0, 0, 0, 0));
		//buttonStyle.active.background = makeTexture ((int)itemBox.width, (int)itemBox.height, new Color(0, 0, 0, 0)); //<-----CRAPPY JUNK
		if (GUI.Button (new Rect (0, 0, itemBox.width, itemBox.height), "", buttonStyle)) {
			selectedItemIndex = itemIndex;
			isItemClicked = true;
			frameDelay = 0;
		}
	}

	private int frameDelay = 0;
	private void itemOnClick () {			
		if (frameDelay < 2) {
			frameDelay ++;
			// itemOption.position = screenToGUICoordinate ((Vector2)Input.mousePosition);			// ideal situation
			itemOption.position = window.position;
			itemOption.position.x += window.width;
		}

		if (selectedItemIndex >= pack.getSize ())
			return;
		
		GUILayout.BeginArea (itemOption.toRect ());
			GUI.Box (new Rect (0, 0, itemOption.width, itemOption.height), "");
			GUIStyle descriptionStyle = new GUIStyle(GUI.skin.box);
			descriptionStyle.alignment = TextAnchor.UpperLeft;
			string description = pack.getItem (selectedItemIndex).getDescription () + "\n價值: " + pack.getItem (selectedItemIndex).getPrice ();
			GUI.Box (new Rect (5, 5, itemOption.width - 10, itemOption.height - 40), description, descriptionStyle);
			createOptionButton ();
		GUILayout.EndArea ();
	}

	private void createOptionButton () {
		if (GUI.Button (new Rect (5, itemOption.height - 30, 40, 20), "使用")) {
			pack.use (selectedItemIndex);
			isItemClicked = false;
		}
		
		if (GUI.Button (new Rect (50, itemOption.height - 30, 40, 20), "丟棄")) {
			pack.removeItem (selectedItemIndex);
			isItemClicked = false;
		}

		if (GUI.Button (new Rect (95, itemOption.height - 30, 40, 20), "關閉")) {
			isItemClicked = false;
		}
	}

	public GUIStyle itemBoxStyleSetting () {
		GUIStyle boxStyle = new GUIStyle(GUI.skin.box);
		boxStyle.alignment = TextAnchor.MiddleLeft;
		boxStyle.font = font;
		boxStyle.fontSize = 12;
		boxStyle.normal.textColor = Color.black;
		return new GUIStyle (boxStyle);
	}

	private void showIcon (Sprite icon) {
		GUI.DrawTexture (new Rect(10, 5, iconSize, iconSize), icon.texture); 										// left boundary: 10, up boundary: 5
	}

	public void scrollByMouseWheel (float value) {
		if (mouseOnWindow ())
			scrollDelta -= value * scrollSpeed;
	}

	private void delay (float sec) {
		float endTime = System.DateTime.UtcNow.Millisecond + sec * 1000;
		while (System.DateTime.UtcNow.Millisecond < endTime)
			;
		return;
	}

	private void createSortButon () {
		if (GUI.Button (new Rect (15, label.height + itemView.height + 20, 50, 50), "Sort"))
			pack.sortbyId ();
	}

	private void createMoneyBar () {
		int money = GameObject.Find ("gameManager").GetComponent<DataLib> ().getMoney ();
		GUIStyle moneyBoxStyle = new GUIStyle (GUI.skin.box);
		moneyBoxStyle.alignment = TextAnchor.MiddleCenter;
		GUI.Box(new Rect(70, label.height + itemView.height + 20, window.width - 50 - 50, 50), "Money : " + money.ToString(), moneyBoxStyle);

	}
}
