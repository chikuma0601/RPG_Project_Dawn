﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class GUIHint : GUIWindow{

	private Font font;
	private bool showHint = false;

	private string title = "提示";
	private string hint;

	void Start() {
		window.height = 100;
		window.width = 250;
		window.position = new Vector2 (Screen.width/2 - window.width/2, Screen.height/2 - window.height/2);

		label.position = Vector2.zero;
		label.width = window.width;
		label.height = 30;

		font = Resources.Load ("Font/NotoSansHant-Light") as Font;
	}

	void OnGUI () {
		if (showHint) {
			// window Area
			GUILayout.BeginArea (window.toRect());
				setBackground ();
				setLabel ();
			GUILayout.EndArea ();
		}
	}

	private void setBackground () {
		GUI.DrawTexture (new Rect (0, label.height, window.width, window.height - label.height), Resources.Load ("BackpackGUI/background") as Texture);

		GUIStyle hintStyle = new GUIStyle(GUI.skin.label);
		hintStyle.font = font;
		hintStyle.fontSize = 15;
		hintStyle.normal.textColor = Color.black;
		hintStyle.alignment = TextAnchor.MiddleCenter;

		GUI.Box (new Rect(0, label.height, window.width, window.height - label.height), hint, hintStyle);

	}

	private void setLabel () {
		GUIStyle labelStyle = new GUIStyle(GUI.skin.label);
		labelStyle.font = font;
		labelStyle.fontSize = 18;
		labelStyle.normal.textColor = Color.black;

		GUI.DrawTexture (label.toRect (), Resources.Load ("BackpackGUI/titleBar") as Texture);

		// place text in the middle of label
		GUILayout.BeginArea (label.toRect());
		GUILayout.BeginHorizontal ();
		GUILayout.FlexibleSpace ();
		GUILayout.BeginVertical ();
		GUILayout.FlexibleSpace ();
		GUILayout.Label (title, labelStyle);
		GUILayout.FlexibleSpace ();
		GUILayout.EndVertical ();
		GUILayout.FlexibleSpace ();
		GUILayout.EndHorizontal ();
		GUILayout.EndArea ();

		setExitButton ();
	}

	private void setExitButton () {
		GUILayout.BeginHorizontal ();
		GUILayout.FlexibleSpace ();
		if (GUILayout.Button ("X", GUILayout.MaxHeight (label.height), GUILayout.MaxWidth (label.height)))
			showHint = false;
		GUILayout.EndHorizontal ();
	}

	public void showHintWindow (string h) {
		hint = h;
		showHint = !showHint;
	}
}

