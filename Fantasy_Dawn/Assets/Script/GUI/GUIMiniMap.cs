﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIMiniMap : MonoBehaviour {
	private BoxSetting miniMapBox;
	private Texture miniMap;
	private Texture playerMark;
	private Texture background;

	private Vector3 mapAnchor;
	private Vector3 miniMapAnchor;
	private float coordinateScalar;

	// Use this for initialization
	void Start () {
		miniMapBox.height = Screen.height / 4f;
		miniMapBox.width = miniMapBox.height;
		miniMapBox.position = new Vector2 (Screen.width - miniMapBox.width - 1, 10);

		miniMap = Resources.Load<Texture> ("Map/forest_02_miniMap");
		playerMark = Resources.Load<Texture> ("Map/playerPosition");
		background = Resources.Load<Texture> ("Map/playerPosition");
	}
	
	// Update is called once per frame
	void OnGUI () {
		GUILayout.BeginArea (miniMapBox.toRect ());
			coordinateChange ();
			GUI.DrawTexture (new Rect (0, 0, miniMapBox.width, miniMapBox.height), background);
			GUI.DrawTexture (new Rect(miniMapAnchor.x, miniMapAnchor.y, miniMapBox.width, miniMapBox.height), miniMap);
			GUI.DrawTexture (new Rect (miniMapBox.width / 2, miniMapBox.height / 2, 5, 5), playerMark);
		GUILayout.EndArea ();
	}

	private void coordinateChange () {
		mapAnchor = GameObject.Find ("background").GetComponent<MapReader> ().getStartAnchor ();
		coordinateScalar = Mathf.Sqrt (miniMapBox.width * miniMapBox.width + miniMapBox.height * miniMapBox.height) / GameObject.Find ("background").GetComponent<MapReader> ().getMapSize ();

		Vector3 playerPositionRelatedToMapAnchor = mapAnchor - GameObject.Find ("player").transform.position;
		playerPositionRelatedToMapAnchor.y *= -1;
		miniMapAnchor = new Vector2 (miniMapBox.width / 2 + playerPositionRelatedToMapAnchor.x * coordinateScalar, miniMapBox.height / 2 + playerPositionRelatedToMapAnchor.y * coordinateScalar);
	}
}
