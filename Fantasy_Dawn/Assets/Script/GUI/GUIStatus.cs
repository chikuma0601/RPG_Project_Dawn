﻿using System;
using UnityEngine;

public class GUIStatus : GUIWindow {
	private Font font;
	private string title;
	private bool showStatus = false;
	private BoxSetting armorBox;
	private BoxSetting weaponBox;
	private BoxSetting characterBox;
	private BoxSetting statusBox;  

	void Start () {
		window.height = Screen.height * 0.8f;
		window.width = window.height / 1.414f;
		window.position = new Vector2 (Screen.width - window.width - 10, 10);

		label.position = Vector2.zero;
		label.width = window.width;
		label.height = 30;

		weaponBox.position = new Vector2 (10, label.height + 10);
		weaponBox.height = window.width / 3 - 10;
		weaponBox.width = weaponBox.height;

		armorBox.position = weaponBox.position;
		armorBox.position.y += weaponBox.height + 10;
		armorBox.height = window.width / 3 - 10;
		armorBox.width = armorBox.height;

		characterBox.position = new Vector2 (window.width / 3 + 10, label.height + 10);
		characterBox.height = (window.height - label.height) / 2 - 10;
		characterBox.width = window.width / 3 * 2 - 20;

		statusBox.position = weaponBox.position;
		statusBox.position.y += characterBox.height + 20;
		statusBox.height = window.height - statusBox.position.y - 10;//(window.height - label.height)/ 2 - 20;
		statusBox.width = window.width - 20;

		font = Resources.Load ("Font/NotoSansHant-Light") as Font;
		title = "狀態";
	
	}

	void OnGUI () {
		if (showStatus) {
			GUILayout.BeginArea (window.toRect ());
				setLabel ();
				setBackground ();
				setWeaponBox ();
				setArmorBox ();
				setCharcterBox ();
				setStatusBox ();
			GUILayout.EndArea ();
		}
	}

	private void setLabel () {
		GUIStyle labelStyle = new GUIStyle(GUI.skin.label);
		labelStyle.font = font;
		labelStyle.fontSize = 18;
		labelStyle.normal.textColor = Color.black;

		GUI.DrawTexture (label.toRect (), Resources.Load ("BackpackGUI/titleBar") as Texture);

		// place text in the middle of label
		GUILayout.BeginArea (label.toRect());
		GUILayout.BeginHorizontal ();
		GUILayout.FlexibleSpace ();
		GUILayout.BeginVertical ();
		GUILayout.FlexibleSpace ();
		GUILayout.Label (title, labelStyle);
		GUILayout.FlexibleSpace ();
		GUILayout.EndVertical ();
		GUILayout.FlexibleSpace ();
		GUILayout.EndHorizontal ();
		GUILayout.EndArea ();

		setExitButton ();
	}

	private void setExitButton () {
		GUILayout.BeginHorizontal ();
		GUILayout.FlexibleSpace ();
		if (GUILayout.Button ("X", GUILayout.MaxHeight (label.height), GUILayout.MaxWidth (label.height))) 
			showStatus = false;
		GUILayout.EndHorizontal ();
	}

	private void setBackground () {
		GUI.DrawTexture (new Rect(0 ,label.height ,window.width ,window.height - label.height), Resources.Load ("BackpackGUI/background") as Texture);
	}

	private void setWeaponBox () {
		GUI.DrawTexture (weaponBox.toRect(), Resources.Load ("temp2") as Texture);
	}

	private void setArmorBox () {
		GUI.DrawTexture (armorBox.toRect(), Resources.Load ("temp2") as Texture);
	}

	private void setCharcterBox () {
		GUI.DrawTexture (characterBox.toRect(), Resources.Load ("BackpackGUI/temp") as Texture);
	}

	private void setStatusBox () {
		string statusString = GameObject.Find ("player").GetComponent<Creature> ().getStatus ();
		GUI.Box (statusBox.toRect (), statusString);
	}

	public void showStatusWindow () {
		showStatus = !showStatus;
	}
}

