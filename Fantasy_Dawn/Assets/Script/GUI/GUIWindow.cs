﻿using System;
using UnityEngine;

public struct BoxSetting {
	public Vector2 position;
	public float width;
	public float height;

	public Rect toRect () {
		return new Rect (position.x, position.y, width, height);
	}
}

public abstract class GUIWindow : MonoBehaviour {

	protected BoxSetting window;
	protected BoxSetting label;


	private Vector2 screenToGUICoordinate (Vector2 mouse){
		mouse = GUIUtility.ScreenToGUIPoint (mouse);
		mouse.y = mouse.y * -1 + Screen.height;
		return mouse;
	}

	protected bool mouseOnWindow () {
		Vector2 mousePosition = screenToGUICoordinate (Input.mousePosition);
		if (mousePosition.x > window.position.x && mousePosition.x < window.position.x + window.width &&
		    mousePosition.y > window.position.y && mousePosition.y < window.position.y + window.height) {
			return true;
		}
		else
			return false;
	}
}

