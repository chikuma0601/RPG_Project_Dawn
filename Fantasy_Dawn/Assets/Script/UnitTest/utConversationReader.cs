﻿using System;
using NUnit.Framework;

/****************************************************************\
 * 																*
 * 																*
 *    Change FilePath In MissionReader && ConversationReader	*
 * 																*
 * 																*
\****************************************************************/ 

[TestFixture]
public class utConversationReader {

	[Test]
	public void utReader () {
		ConversationReader reader = new ConversationReader ();
		ConversationNode treeTop = reader.read (0);

		//Assert.AreEqual ("", treeTop.getDialogueList [0]);
		Assert.AreEqual(4, treeTop.getDialogueList().Count);
		Assert.AreEqual (2, treeTop.optionSize ());

		Assert.AreEqual ("諾亞・古德里安: 最近我們邊境領附近出現了許多的史萊姆", treeTop.getDialogueList () [0].toString ());
		Assert.AreEqual ("諾亞・古德里安: 不知道你可不可以協助我們清除這些東西？", treeTop.getDialogueList () [1].toString ());
		Assert.AreEqual ("諾亞・古德里安: 這些有著黏液的古怪生物常常在我們不注意的時候就黏上來", treeTop.getDialogueList () [2].toString ());
		Assert.AreEqual ("諾亞・古德里安: 實在是讓人有些不舒服", treeTop.getDialogueList () [3].toString ());

		Assert.AreEqual ("好的", treeTop.getOption (0).getOptionString ());
		Assert.AreEqual (1, treeTop.getOption (0).getNextConversationCode ());
		Assert.AreEqual ("我目前還沒有名字: 好吧，將讓我來協助你們吧", treeTop.nextNode (0).getDialogueList () [0].toString());
		Assert.AreEqual ("諾亞・古德里安: 真的嗎？謝謝你！", treeTop.nextNode (0).getDialogueList () [1].toString());
		Assert.AreEqual ("諾亞・古德里安: 那就拜託你了！", treeTop.nextNode (0).getDialogueList () [2].toString());
		Assert.AreEqual (1, treeTop.nextNode (0).getEventSize ());
		Assert.AreEqual ("第一王旗", ((KillTask)treeTop.nextNode (0).getEvent (0)).getMissionName ());

		Assert.AreEqual ("不行", treeTop.getOption (1).getOptionString ());
		Assert.AreEqual (0, treeTop.getOption (1).getNextConversationCode ());
		Assert.AreEqual ("我目前還沒有名字: 不好意思，我對史萊姆這種生物有點過敏⋯⋯", treeTop.nextNode (1).getDialogueList () [0].toString());
	}

	[Test]
	public void utReader1 () {
		ConversationReader reader = new ConversationReader ();
		ConversationNode treeTop = reader.read (1);

		Assert.AreEqual (0, treeTop.getMissionConstraint ().getCode());
		try {
			treeTop.getMissionConstraint ().getNextCode();
			Assert.Fail();
		} catch (Exception e) {
			Assert.AreEqual ("Requirement achieve status: False", e.Message);
		}

	}
}


