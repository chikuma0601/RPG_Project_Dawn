﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

public class utSlime {

	public void setUp () {
		UnityEngine.SceneManagement.SceneManager.LoadScene ("Dawn_RPG");
	}

	[Test]
	public void utSlimeSimplePasses() {
		// Use the Assert class to test conditions.
	}

	// A UnityTest behaves like a coroutine in PlayMode
	// and allows you to yield null to skip a frame in EditMode

	[UnityTest]
	public IEnumerator slimeCreate() {
		// Use the Assert class to test conditions.
		// yield to skip a frame
		setUp();
		yield return new WaitForFixedUpdate ();
		Slime [] slime = GameObject.FindObjectsOfType<Slime> ();
		Assert.AreEqual (1, slime.Length);

		foreach (Slime s in slime) {
			Assert.AreEqual (100, s.getHp);
			Assert.AreEqual (100, s.getHp);
			Assert.AreEqual (20, s.getAtk);
			Assert.AreEqual (10, s.getDef);
		}

		yield return null;
	}

	[UnityTest]
	public IEnumerator slimeFindPlayer () {
		setUp ();
		yield return new WaitForSecondsRealtime (2f);
		Slime[] slime = GameObject.FindObjectsOfType<Slime> ();
		foreach (Slime s in slime) {
			Assert.AreEqual ("Player", s.target.ToString());
		}
	}
}
