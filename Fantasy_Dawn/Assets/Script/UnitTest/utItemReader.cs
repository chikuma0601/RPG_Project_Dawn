﻿using System;
using NUnit.Framework;

[TestFixture]
public class utItemReader {
	[Test]
	public void utCode_0 () {
		ItemReader reader = new ItemReader ();
		Item item = reader.read ("0");

		Assert.AreEqual (typeof(Weapon), item.GetType ());
		Weapon weapon = item as Weapon;

		Assert.AreEqual (100, weapon.getDamage ());
		Assert.AreEqual ("普通的劍", weapon.getName ());
		Assert.AreEqual ("普通的鑄造長劍，並沒有什麼特別之處。", weapon.getDescrition ());
	}

	[Test]
	public void utCode_1 () {
		ItemReader reader = new ItemReader ();
		Item item = reader.read ("1");

		Assert.AreEqual (typeof(Weapon), item.GetType ());
		Weapon weapon = item as Weapon;

		Assert.AreEqual (1000, weapon.getDamage ());
		Assert.AreEqual ("帝國一式配發長刀", weapon.getName ());
		Assert.AreEqual ("帝國配發給軍人的長刀，刀鍔的部分刻著配發的年份以及帝國的國徽。", weapon.getDescrition ());
	}

	[Test]
	public void utCode_100 () {
		ItemReader reader = new ItemReader ();
		Item item = reader.read ("100");

		Assert.AreEqual (typeof(Potion), item.GetType ());
		Potion drug = item as Potion;

		// effext : too lazy to test :(
		Assert.AreEqual ("普通回復藥", drug.getName ());
		Assert.AreEqual ("人類藥學上的奇蹟，喝下後能夠迅速的恢復體力以及癒合傷口。對感冒風寒等病症無效。", drug.getDescrition ());
	}

}

