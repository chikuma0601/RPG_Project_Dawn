﻿using System;
using NUnit.Framework;
using System.Collections.Generic;

[TestFixture]
public class utConversationNode
{
	[Test]
	public void NodeConstruct() {
		string[] name = new string [] { "a", "b", "c", "d", "e" };
		string[] sentence = new string [] { "1", "2", "3", "4", "5" };
		List<Dialogue> d = new List<Dialogue> ();
		for (int i = 0; i < name.Length; ++i)
			d.Add (new Dialogue (name [i], sentence [i]));

		Option[] op = new Option[2];
		op [0] = new Option ("op1", null, 0);
		op [1] = new Option ("op2", null, 1);

		ConversationNode tree = new ConversationNode (d);

		tree.addOption (op [0]);
		tree.addOption (op [1]);

		for (int i = 0; i < name.Length; ++i)
			Assert.AreEqual (name [i] + ": " + sentence [i], tree.getDialogueList () [i].ToString ());

		Assert.AreEqual (null, tree.nextNode (0));
		Assert.AreEqual (null, tree.nextNode (1));
		Assert.AreEqual (0, tree.getOption (0).getNextConversationCode ());
		Assert.AreEqual (1, tree.getOption (1).getNextConversationCode ());

	}

	[Test]
	public void MoreNodeConstruct() {
		string[] name = new string [] { "a", "b", "c", "d", "e" };
		string[] sentence = new string [] { "1", "2", "3", "4", "5" };
		List<Dialogue> d = new List<Dialogue> ();
		for (int i = 0; i < name.Length; ++i)
			d.Add (new Dialogue (name [i], sentence [i]));

		Option[] op = new Option[2];
		op [0] = new Option ("op1", null, 0);
		op [1] = new Option ("op2", null, 0);

		ConversationNode treeLeft = new ConversationNode (d);
		ConversationNode treeRight = new ConversationNode (d);

		treeLeft.addOption (op [0]);
		treeRight.addOption (op [0]);

		op [0] = new Option ("op1", treeLeft, 0);
		op [1] = new Option ("op2", treeRight, 0);

		ConversationNode tree = new ConversationNode (d);

		tree.addOption (op [0]);
		tree.addOption (op [1]);

		for (int i = 0; i < name.Length; ++i)
			Assert.AreEqual (name [i] + ": " + sentence [i], tree.getDialogueList () [i].ToString ());

		Assert.AreSame (treeLeft, tree.nextNode (0));
		Assert.AreSame (treeRight, tree.nextNode (1));

	}

	[Test]
	public void utAddEvent () {
		string[] name = new string [] { "a", "b", "c", "d", "e" };
		string[] sentence = new string [] { "1", "2", "3", "4", "5" };
		List<Dialogue> d = new List<Dialogue> ();
		for (int i = 0; i < name.Length; ++i)
			d.Add (new Dialogue (name [i], sentence [i]));

		Option[] op = new Option[2];
		op [0] = new Option ("op1", null, 0);
		op [1] = new Option ("op2", null, 0);

		ConversationNode tree = new ConversationNode (d);
		tree.addEvent (null);
		Assert.AreEqual(0, tree.getEventSize());

		tree.addEvent (new KillTask (0, "testmission", new Dictionary<string, int>(), new List<Reward>()));
		Assert.AreEqual (1, tree.getEventSize ());
	}

}