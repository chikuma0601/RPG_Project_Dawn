﻿using System;
using NUnit.Framework;

[TestFixture]
public class utOption
{
	[Test]
	public void optionConstruct() {
		Option op = new Option ("YAHOOO", null, 1);
		Assert.AreEqual ("YAHOOO", op.getOptionString ());
		Assert.AreEqual (null, op.next ());
		Assert.AreEqual (1, op.getNextConversationCode());
	}
}