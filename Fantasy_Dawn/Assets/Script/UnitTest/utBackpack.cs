﻿using System.Collections.Generic;
using NUnit.Framework;
using System.Collections;

[TestFixture]
public class utBackpack
{
	[Test]
	public void backpackSort () {
		Potion p = new Potion (3, "potion", new List<Buff>(), "來路不明的紫色藥水", 3);
		Weapon w1 = new Weapon (1, "sword", 100, "inin", 0);
		Weapon w2 = new Weapon (2, "lance", 100, "der", 0);

		Backpack bp = new Backpack ();

		bp.addItem (p);
		bp.addItem (w1);
		bp.addItem (w2);

		bp.sortbyId ();

		Assert.AreEqual (1, bp.getItem (0).getItemCode ());
		Assert.AreEqual (2, bp.getItem (1).getItemCode ());
		Assert.AreEqual (3, bp.getItem (2).getItemCode ());
	}
}


