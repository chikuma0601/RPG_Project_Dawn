﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct State {
	public string name;
	public int hp;
	public int fullHp;
	public float atk;
	public int def;
	public float attackSpeed;
	public float movingSpeed;
	public override string ToString () {
		return toString ();
	}
	public string toString() {
		string s = 
			"name: " + name +
			"\nhp: " + hp +
			"\natk: " + atk +
			"\ndef: " + def +
			"\nattackSpeed: " + attackSpeed +
			"\nmovingSpeed: " + movingSpeed;
		return s;
	}
};

public class Creature : MonoBehaviour {


	public State status;
	[SerializeField]
	public string stateString;
	protected Rigidbody2D rb;
	protected string direction = "down";
	public string hostile;
	public List<Buff> buffList = new List<Buff> ();

	public int getHp{
		get{
			return status.hp;
		} set {
			status.hp = value;
		}
	}

	public float getAtk{
		get{
			return status.atk;
		} set {
			status.atk = value;
		}
	}

	public int getDef{
		get{
			return status.def;
		} set {
			status.def = value;
		}
	}

	public int getFullHp{
		get{
			return status.fullHp;
		} set {
			status.fullHp = value;
		}
	}

	public string getStatus () {
		return stateString;
	}

	public string getName () {
		return status.name;
	}

	public void sortingLayer () {
		GetComponent<SpriteRenderer> ().sortingOrder = -Mathf.RoundToInt (transform.position.y * 10f);
	}

	public string getHostile () {
		return hostile;
	}
		
	public void killCreature (Creature c) {
		Destroy (c.gameObject);
	}

	public void gainBuff (Buff buff) {
		buffList.Add (buff);
		switch (buff.name) {
		case "Atk":
			getAtk += ((Atk)buff).effect;
			break;
		case "Def":
			getDef += ((Def)buff).effect;	
			break;
		case "Poison":
			// do something
			break;
		case "Stun":
			rb.constraints = RigidbodyConstraints2D.FreezeAll;
			break;
		case "Slow":
			status.movingSpeed *= ((Slow)buff).effect;
			break;
		case "Recover":
			getHp += (int) ((Recover)buff).effect;
			break;
		case "KnockBack":
			knockBack ((KnockBack)buff);
			break;
		default:
			throw new UnmatchException ("Unknown buff");
		}
	}

	public IEnumerator checkBuffDuration () {
		while (true) {
			yield return new WaitForSecondsRealtime (0.5f);
			for (int i = 0; i < buffList.Count; ++i) {
				buffList [i].buffDuration -= 0.5f;
				continuousEffect (buffList [i]);
				if (buffList [i].buffDuration <= 0) {
					checkBuffEnd (i);
					buffList.RemoveAt (i);
				}
			}
		}
	}

	public void checkBuffEnd (int i) {
		switch (buffList[i].name) {
			case "Atk":
				getAtk -= ((Atk)buffList[i]).effect;
				break;
			case "Def":
				getDef -= ((Def)buffList[i]).effect;	
				break;
			case "Stun":
				rb.constraints = RigidbodyConstraints2D.FreezeRotation;
				break;
			case "Slow":
				status.movingSpeed /= ((Slow)buffList[i]).effect;
				break;
		}
	}

	public void continuousEffect (Buff b) {
		switch (b.name) {
		case "Poison":
			getHp -= Mathf.RoundToInt(((Poison)b).effect);
			break;
		}
	}

	public void changeAppearance (Animator anim, string name) {
		if (rb.velocity == Vector2.zero) {
			anim.Play (name + "_idle_" + direction);
		}
		else
			anim.Play (name + "_move_" + direction);
	}

	public void knockBack (KnockBack effect) {
		Vector3 attackerPoint = effect.getAttacker ().transform.position;
		Vector3 myPoint = this.gameObject.transform.position;
		Vector3 vec = myPoint - attackerPoint;

		print ("attackerPoint: " + attackerPoint.ToString ());
		print ("myPoint: " + myPoint.ToString ());
		print ("The vector sub: " + (vec*1000).ToString ());
		print ("This is me: " + this.gameObject.ToString ());
		//bad way, use rigidbidy2D insead.
		rb.freezeRotation = true;
		rb.velocity = vec * 1000;
		//this.gameObject.transform.Translate (myPoint + vec);

		/* Issues : this knock back will only activate randomly? */
	}
}
