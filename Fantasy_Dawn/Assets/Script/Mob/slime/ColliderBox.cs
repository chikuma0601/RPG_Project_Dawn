﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderBox : MonoBehaviour {

	void OnCollisionEnter2D (Collision2D collide) {
		if (collide.collider.CompareTag ("Player_collider")) {
			GetComponentInParent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.FreezeAll;
			transform.parent.GetComponent<Mob> ().setAttack (true);
		}
	}

	void OnCollisionExit2D (Collision2D collide) {
		if (collide.collider.CompareTag ("Player_collider")) {
			GetComponentInParent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.FreezeRotation;
			transform.parent.GetComponent<Mob> ().setAttack (false);
		}
	}
}
