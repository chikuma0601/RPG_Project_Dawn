﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slime : Mob{

	private Animator anim;

	// Use this for initialization
	void Start () {
		getHp = 100;
		getAtk = 20;
		getDef = 10;
		status.movingSpeed = 5f;
		status.attackSpeed = 1f;
		alertRange = 5;
		rb = GetComponent<Rigidbody2D> ();
		me = this.gameObject;
		status.name = "slime_green";
		hostile = "Player";
		originalPosition = transform.position;
		direction = "left";
		anim = GetComponent<Animator> ();
		anim.Play ("slime_green_idle_" + direction);
		drops.money = (int)Random.Range(100f, 150f);
		drops.exp = 100;
		drops.item = new Dictionary<Item, int> ();
		drops.item.Add (new Material(300, "胖次", "史萊姆掉的胖次（？）", 10000), 50);
		drops.item.Add (new Material(301, "史萊姆的黏液", "黏黏的史萊姆掉的黏黏的黏液", 10000), 100);
		drops.item.Add (new Material(302, "奇怪的胖次", "史萊姆變生成的胖次（？）", 10000), 1);
		drops.item.Add (new Material(303, "尼洛伊德", "冰涼的飲品（涮涮的口感？）", 10000), 50);


		StartCoroutine (checkBuffDuration ());
		//StartCoroutine(randomMoving());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate () {
		stateString = status.toString ();
		sortingLayer ();
		findPlayerInRange (alertRange);
		activate ();
		changeDirection ();
		changeAppearance (anim, status.name);
		if (startAttack && !isAttacking) {
			StartCoroutine (attack ());
		}
	}

	private void changeDirection () {
		if (rb.velocity.x > 0)
			direction = "right";
		if (rb.velocity.x < 0)
			direction = "left";
	}

	private void activate () {
		if (foundPlayer) {
			rb.velocity = (target.transform.position - this.transform.position).normalized * status.movingSpeed;
		} else if (!isRandomMoving) {
			//rb.velocity = Vector2.zero;
			StartCoroutine(randomMoving());
		}
	}

	IEnumerator attack () {
		// anim.play("attack");
		isAttacking = true;
		GameObject ar = transform.Find("attackRange").gameObject;
		ar.GetComponent<AttackRange> ().skill = new NormalAttack (1/status.attackSpeed, this.gameObject);
		ar.SetActive (true);
		yield return new WaitForSecondsRealtime (ar.GetComponent<AttackRange> ().skill.cd);
		ar.SetActive (false);
		isAttacking = false;
	}

}
