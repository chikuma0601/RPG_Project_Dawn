﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Astolfo : Mob {

	// Use this for initialization
	void Start () {
		getHp = 1000;
		getAtk = 100;
		getDef = 100;
		status.movingSpeed = 5f;
		status.attackSpeed = 1f;
		alertRange = 5;
		rb = GetComponent<Rigidbody2D> ();
		me = this.gameObject;
		status.name = "astolfo";
		hostile = "Player";

		StartCoroutine (checkBuffDuration ());
	}

	// Update is called once per frame
	void Update () {

	}

	void FixedUpdate () {
		stateString = status.toString ();
		sortingLayer ();
		findPlayerInRange (alertRange);
		activate ();

		if (startAttack && !isAttacking) {
			StartCoroutine (attack ());
		}
	}

	private void activate () {
		if (foundPlayer) {
			rb.velocity = (target.transform.position - this.transform.position).normalized * status.movingSpeed;
		} else {
			rb.velocity = Vector2.zero;
		}
	}
		
	IEnumerator attack () {
		// anim.play("attack");
		isAttacking = true;
		GameObject ar = transform.Find("attackRange").gameObject;
		ar.GetComponent<AttackRange> ().skill = new NormalAttack (1/status.attackSpeed,this.gameObject);
		ar.SetActive (true);
		yield return new WaitForSecondsRealtime (ar.GetComponent<AttackRange> ().skill.cd);
		ar.SetActive (false);
		isAttacking = false;
	}
}
