﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Drops {
	public int exp;
	public int money;
	//public List<Item> item;
	public Dictionary<Item, int> item;
};

public class Mob : Creature {
	
	protected bool foundPlayer = false;
	protected bool startAttack = false;
	protected bool isAttacking = false;
	protected float alertRange;
	protected Vector3 originalPosition;
	protected Vector3 destination = new Vector3 ();
	protected float movingTime = 0;
	protected bool isRandomMoving = false;
	protected GameObject me;
	public GameObject target;
	protected Drops drops; 


	//this is an interface !

	protected float getDistanceToPlayer () {
		float distance;
		distance = (target.transform.position - me.transform.position).magnitude;
		return distance;
	}

	protected bool isPlayerFound () {
		return foundPlayer;
	}

	public void findPlayerInRange (float range) {
		target = GameObject.FindGameObjectWithTag ("Player");
		if (getDistanceToPlayer () < alertRange) {
			foundPlayer = true;
		} else {
			foundPlayer = false;
		}
	}

	public void setAttack (bool trigger) {
		startAttack = trigger;
	}

	public IEnumerator randomMoving () {
		isRandomMoving = true;
		rb.velocity = Vector2.zero;
		if (movingTime <= 0) {	// when stopped
			destination = originalPosition + new Vector3 (Random.Range(-5, 5), Random.Range (-5, 5));
			yield return new WaitForSecondsRealtime (1f);
			movingTime = Random.Range (0, 3);
		} else {
			whenMoving ();
			yield return null;
		}
		isRandomMoving = false;
	}

	private void whenMoving () {
		if ((destination - transform.position).magnitude >= 0.05) {
			rb.velocity = (destination - transform.position).normalized * status.movingSpeed;
			movingTime -= Time.fixedDeltaTime;
		} else {
			movingTime = 0;
		}
	}

	public Drops getDrops () {
		return drops;
	}
}