﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour {

	public string npcName;
	public List<string> filePath;
	protected int conversationCode = 0;


	public void sortingLayer () {
		GetComponentInParent<SpriteRenderer> ().sortingOrder = -Mathf.RoundToInt (transform.position.y * 10f);
	}

	public void changeConversation (int code) {
		conversationCode = code;
	}

}
