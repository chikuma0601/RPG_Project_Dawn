﻿using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NoName : NPC {
	public Text dialogueText;

	void Start () {
		npcName = "めぐみん";
		dialogueText.gameObject.SetActive (false);
	}

	void FixedUpdate () {
		sortingLayer ();
	}

	void OnTriggerEnter2D (Collider2D collide) {
		if (collide.CompareTag ("Player_collider")) {
			dialogueText.gameObject.SetActive (true);
			GameObject.Find("gameManager").GetComponent<DialogueController> ().init (conversationCode, this, "Text/vendorNPC_00");
		}
	}

	void OnTriggerExit2D (Collider2D collide) {
		if (collide.CompareTag ("Player_collider")) {
			GameObject.Find ("gameManager").GetComponent<DialogueController> ().exitTalkingRange ();
			dialogueText.gameObject.SetActive (false);
		}
	}

}
