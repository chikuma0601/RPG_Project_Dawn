﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : Creature {
	
	Animator playerAnimation;
	public Transform attackRange;
	public Text gameOverText;
	private Dictionary<string, float> cdTime = new Dictionary<string, float> () {
		{ "normalAttack", 0 },
		{ "attackWithPoison", 0 },
		{ "heavyAttack", 0 }
	};

	private Equipment equipment = new Equipment ();
	// Use this for initialization
	void Start () {
		playerAnimation = GetComponent<Animator> ();
		playerAnimation.Play ("player_idle_down");
		rb = GetComponent<Rigidbody2D> ();
		getHp = 1000;
		getFullHp = 1000;
		getAtk = 50;
		getDef = 10;
		direction = "down";
		status.movingSpeed = 10f;
		status.attackSpeed = 2f;
		status.name = "player";
		hostile = "Monster";
		gameOverText.gameObject.SetActive(false);

		StartCoroutine (checkBuffDuration ());	// A bit scary
		StartCoroutine(cdCountDown());

	}
	
	// Update is called once per frame
	void Update () {
		//StartCoroutine (attack());
	}

	void FixedUpdate () {
		stateString = status.toString ();
		changeAppearance (playerAnimation, status.name);
		sortingLayer ();
		Dead ();
	}

	public void move (string moveDirection) {
		switch (moveDirection) {
		case "up":
			direction = "up";
			rb.velocity = Vector2.up * status.movingSpeed;
			break;
		case "down":
			direction = "down";
			rb.velocity = Vector2.down * status.movingSpeed;
			break;
		case "left":
			direction = "left";
			rb.velocity = Vector2.left * status.movingSpeed;
			break;
		case "right":
			direction = "right";
			rb.velocity = Vector2.right * status.movingSpeed;
			break;
		case "stop":
			rb.velocity = Vector2.zero;
			break;
		default:
			throw new UnmatchException ("moveDirection error");
		}
	}

	public IEnumerator attack (string skillName) {
		if (cdTime [skillName] <= 0) {
			GameObject range = createAttackRange ();
			range.GetComponent<AttackRange> ().skill = createSkill (skillName);
			cdTime [skillName] = range.GetComponent<AttackRange> ().skill.cd;

			// playerAnimation.Play ("");
			//yield return new WaitForSecondsRealtime (0.3f); // animation duration == 0.3f 
			yield return new WaitForFixedUpdate();
			Destroy (range);
		}
	}

	private Skill createSkill (string skillName) {
		switch (skillName) {
		case "normalAttack": 
			return new NormalAttack (1/status.attackSpeed, this.gameObject);	// need some testing
		case "attackWithPoison": 
			return new AttackWithPoison (5, 10, 5);
		case "heavyAttack": 
			return new HeavyAttack (5, 5);
		default:
			throw new UnmatchException ("unmatch skill");
		}
	}

	//public 

	public IEnumerator cdCountDown () {
		while (true) {
			yield return new WaitForSecondsRealtime (0.3f);
			List<string> keys = new List<string> (cdTime.Keys);
			foreach (string key in keys) {
				if (cdTime[key]> 0)
					cdTime [key] -= 0.3f;
			}
		}
	}

	public GameObject createAttackRange () {
		return Instantiate (attackRange.Find ("attackRange_" + direction + "_swing"), transform).gameObject;
	}

	public void Dead () {
		if (getHp <= 0) {
			//PlayerAnimation.Play ("Player_dead");	//don't loop this animation
			//this.killCreature (this);
			gameOverText.gameObject.SetActive(true);
			//player can still move (need more discussion)
		}
	}

	public void levelUp() {
		// playerAnimation.Play ("levelUp");
		status.atk += 2;
		status.def += 1;
		status.hp += 10;
		status.fullHp += 10;
	}

	public void equip (Weapon weapon) {
		if (equipment.WeapomEquipped())
			unEquip ("weapon");
		equipment.equip (weapon);
		status.atk += weapon.getDamage ();
	}

	public void equip (Armor armor) {
		if (equipment.ArmorEquipped())
			unEquip ("armor");
		equipment.equip (armor);
		status.def += armor.getDefend ();
	}

	public void unEquip (string type) {
		switch (type) {
		case "weapon":
			status.atk -= equipment.getWeaponAtk ();
			equipment.unEquipWeapon ();
			break;
		case "armor":
			status.atk -= equipment.getArmorDef ();
			equipment.unEquipArmor ();
			break;
		default:
			throw new UnmatchException ("Unkonwn equiptment type: " + type);
		}
	}

}