﻿using System;
using UnityEngine;

public abstract class Constraint {
	protected bool isFree = false;

	public bool getConstrainStatus () {
		return isFree;
	}

	public void utSetComplete () {
		isFree = true;
	}
}

public class MissionConstraint : Constraint {
	private int missionCode = -1, nextConversationCode;

	public MissionConstraint (int code, int nextCode) {
		missionCode = code;
		nextConversationCode = nextCode;
	}

	public int getCode () {
		return missionCode;
	}

	public int getNextCode () {
		if (isFree)
			return nextConversationCode;
		else
			throw new RequireNotMatchException ("Requirement achieve status: " + isFree);
	}

	public void setCode (int code) {
		missionCode = code;
	}

	public void setNextCode (int code) {
		nextConversationCode = code;
	}

	public void ifRequireAchieve () {
		isFree = GameObject.Find("gameManager").GetComponent<DataLib>().missionCtrl.checkIfCompleted (missionCode);
	}
}
