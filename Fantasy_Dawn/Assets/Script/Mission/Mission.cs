﻿using System;
using System.Collections;
using System.Collections.Generic;

public struct Reward {
	public string type;
	public int value;

	public Reward (string type, int value) {
		this.type = type;
		this.value = value;
	}
}
/**
 * Mission
 * Interface of all kinds of missions
 */

public abstract class Mission : Event{
	public string name;
	public List<Reward> rewardList = new List<Reward> ();  
	protected int missionCode;

	public string getMissionName () {
		return name;
	}

	public int getMissionCode () {
		return missionCode;
	}

	public abstract bool isComplete ();
	public abstract string toString ();
}
