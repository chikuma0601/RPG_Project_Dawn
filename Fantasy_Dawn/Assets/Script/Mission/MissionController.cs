﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionController {
	List<Mission> missionList = new List<Mission> ();

	public void showMissionList () {
		if (missionList.Count == 0)
			MonoBehaviour.print ("null");
		foreach (Mission m in missionList)
			MonoBehaviour.print (m.ToString());
	}

	public void addMission (Mission m) {
		if (!inList (m.getMissionCode ())) {
			missionList.Add (m);
			MonoBehaviour.print ("mission: " + m.getMissionName() + " add");
			MonoBehaviour.print ("mission code = " + m.getMissionCode());
		}
		else
			MonoBehaviour.print ("mission already in list");
	}

	public bool inList (int code) {
		foreach (Mission m in missionList)
			if (m.getMissionCode() == code)
				return true;
		return false;
	}

	public void removeMission (int code) {
		for (int i = 0; i < missionList.Count; ++i)
			if (missionList [i].getMissionCode () == code)
				missionList.RemoveAt (i);
	}

	public bool checkIfCompleted (int code) {
		MonoBehaviour.print ("Mission List count : " + missionList.Count);
		foreach (Mission m in missionList) {
			if (m.getMissionCode () == code)
				return m.isComplete ();
		}
		throw new Exception ("Mission by code not in List, code:" + code);
	}

	public void killEvent (string name) {
		foreach (Mission m in missionList)
			if (m is KillTask)
				((KillTask)m).check(name);
	}

	public int getMissionIndexByCode (int code) {
		inList (code);
		for (int i = 0; i < missionList.Count; ++i)
			if (missionList [i].getMissionCode () == code)
				return i;
		throw new CrappyException ("Something horrible happened");
	}

	public void getMissionReward (int code) {
		int index = getMissionIndexByCode (code);
		foreach (Reward reward in missionList[index].rewardList)
			switch (reward.type) {
			case "item":
				GameObject.Find ("gameManager").GetComponent<DataLib> ().inventory.addItem (createItem (reward.value));
				break;
			case "exp":
				GameObject.Find ("gameManager").GetComponent<DataLib> ().addExp (reward.value);
			break;
			case "money":
				GameObject.Find ("gameManager").GetComponent<DataLib> ().addMoney (reward.value);
				break;
			default :
			throw new UnmatchException ("Not matching reward type: " + reward.type);
			}
			
	}

	private Item createItem (int code) {
		string ItemXml = GameObject.Find ("gameManager").GetComponent<DataLib> ().itemXml;
		ItemReader reader = new ItemReader (ItemXml);
		return reader.read (code.ToString());
	}

	public List<Mission> getMissionList () {
		return missionList;
	}
}
