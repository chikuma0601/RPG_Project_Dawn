﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using System;

/**
 * KillTask
 * Defines the killing mission target(dictionary)
 */
public class KillTask : Mission {
	private Dictionary<string, int> target;
	private Dictionary<string, int> progress = new Dictionary<string, int> ();

	public KillTask (int code, string name, Dictionary<string, int> targetDictionary, List<Reward> rewards) {
		missionCode = code;
		this.name = name;
		this.target  = new Dictionary<string, int> (targetDictionary);
		initProgress ();
		this.rewardList = new List<Reward> (rewards);
	}

	public override string ToString () {
		return toString ();
	}

	public override string toString () {
		string s = "missionCode: " + missionCode + "\tname: " + name + "\ntype: KillTask\n";
		foreach (KeyValuePair<string, int> i in target)
			s += "target name: "+ i.Key + "\tnumber: " + progress[i.Key] + "/" + target[i.Key] + "\n";
		foreach (Reward reward in rewardList)
			s += "rewardType: " + reward.type + ", rewardValue: " + reward.value + "\n";
		return s;
	}

    public Dictionary<string, int> getDictionary () {
        return target;
    }

	public void initProgress () {
		foreach (KeyValuePair<string, int> t in target)
			progress.Add (t.Key, 0);
	}

	public void check (string key) {
		if (progress.ContainsKey (key) && progress[key] < target[key])
			progress [key]++;
	}

	public override bool isComplete () {
		foreach (KeyValuePair<string, int> i in target)
			if (target [i.Key] != progress [i.Key])
				return false;
		return true;
	}

}
