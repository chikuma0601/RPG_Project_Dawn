﻿using System.Collections.Generic;
using System.Xml;
using System;
using UnityEngine;

public class MissionReader {
	private XmlDocument taskFile = new XmlDocument ();
	private XmlNode task;

	private string filePath = "../../../Assets/Resources/Task/task.xml";		// for ut

	public MissionReader () {}
	public MissionReader (string filePath) {
		this.filePath = filePath;
	}


	public Mission read (string code) {
		try {
			taskFile.LoadXml (filePath);
		}
		catch (XmlException e) {
			taskFile.Load (filePath);
		}

		task = taskFile.SelectSingleNode ("mission/missionCode[attribute::code=" + code + "]");
		if (task == null)
			throw new UnmatchException("unmatch mission code");
		return createMission (Convert.ToInt32(code));
	}

	public Mission createMission (int code) {
		switch (task.SelectSingleNode("missionType").Attributes["type"].Value) {
		case "KillTask":
			return createKillTask (code);
		default:
			throw new UnmatchException("unknown missionType: " + task.SelectSingleNode("missionType").ToString());
		}
	}

	public KillTask createKillTask (int code) {
		Dictionary<string, int> targetDictionary = new Dictionary<string, int> ();
		foreach (XmlNode target in task.SelectNodes("target"))
			targetDictionary.Add (target.Attributes ["mob"].Value, Convert.ToInt32 (target.Attributes ["number"].Value));
		return new KillTask (code, task.Attributes ["name"].Value, targetDictionary, readReward ());
	}

	private List<Reward> readReward () {
		List<Reward> rewardList = new List<Reward> ();
		foreach (XmlNode rewardNode in task.SelectSingleNode("reward").ChildNodes){
			rewardList.Add (new Reward (rewardNode.Name, Convert.ToInt32 (rewardNode.Attributes ["value"].Value)));
			//MonoBehaviour.print ("rewardType: " + rewardNode.Name + ", rewardValue: " + rewardNode.Attributes ["value"].Value);
		}
		return new List<Reward> (rewardList);
	}
}
