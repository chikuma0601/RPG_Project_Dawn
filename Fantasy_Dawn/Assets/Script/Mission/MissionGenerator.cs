﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.IO;

public class MissionGenerator {
    private XmlDocument taskFile = new XmlDocument();
    //private readonly string filePath = "task.xml";
    private readonly string filePath = "../../../../Fantasy_Dawn/Assets/Task/task.xml";
    private XmlElement missionCode;
    private Mission m;

    public MissionGenerator (Mission task) {
        taskFile.Load(filePath);
        m = task;
        int size = Convert.ToInt32(taskFile.SelectSingleNode("mission").Attributes["size"].Value);
        setMissionName(size.ToString(), task.name);
        XmlElement missionDetail = taskFile.CreateElement("missionType");
        missionDetail.SetAttribute("type", task.GetType().ToString());
        missionCode.AppendChild(missionDetail);

        setMission(missionDetail);
        taskFile.SelectSingleNode("mission").Attributes["size"].Value = (size + 1).ToString();
        taskFile.Save(filePath);
    }

    public void setMissionName (string code, string name) {
		missionCode = taskFile.CreateElement("missionCode");
		missionCode.SetAttribute("code", code);
        missionCode.SetAttribute("name", name);
		taskFile.SelectSingleNode("mission").AppendChild(missionCode);

    }

    public void setMission (XmlElement missionDetail) {
		switch (m.GetType().ToString()){
			case "KillTask":
                setKillTaskDetail(missionDetail);
				break;
			default:
				break;
		}
    }

    public void setKillTaskDetail (XmlElement missionDetail) {
		foreach (KeyValuePair<string, int> target in ((KillTask)m).getDictionary()){
			missionDetail = taskFile.CreateElement("target");
			missionDetail.SetAttribute("mob", target.Key);
			missionDetail.SetAttribute("number", target.Value.ToString());
			missionCode.AppendChild(missionDetail);
		}
    }
}
