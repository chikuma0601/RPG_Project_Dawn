﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HpLine : MonoBehaviour {
	public GameObject player;
	private float fullHp;
	private float currentHp;
	RectTransform rt;
	// Use this for initialization
	void Start () {
		fullHp = player.GetComponent<PlayerController> ().getHp;
		rt = GetComponent<RectTransform> ();
	}
	
	// Update is called once per frame
	void LateUpdate () {
		fullHp = player.GetComponent<PlayerController> ().getFullHp;
		currentHp = player.GetComponent<PlayerController> ().getHp;
		rt.sizeDelta = new Vector2 (100 * (currentHp / fullHp), 10);	//need to fix...
	}
}
