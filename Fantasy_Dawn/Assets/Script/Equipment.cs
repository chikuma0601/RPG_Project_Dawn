﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equipment {
	private Weapon weapon;
	private Armor armor;

	public void equip (Weapon w) {
		weapon = w;
	}

	public void equip (Armor a) {
		armor = a;
	}

	public void unEquipWeapon () {
		weapon = null;
	}

	public void unEquipArmor () {
		armor = null;
	}

	public int getWeaponAtk () {
		return weapon.getDamage ();
	}

	public int getArmorDef () {
		return armor.getDefend ();
	}

	public string toString () {
		string s = "Equipment: \n";
		s += "weapon: " + weapon.getName () +", armor: " + armor.getName ();
		return s;
	}

	public override string ToString () {
		return toString();
	}

	public bool WeapomEquipped () {
		return weapon != null;
	}

	public bool ArmorEquipped () {
		return armor != null;
	}
}
