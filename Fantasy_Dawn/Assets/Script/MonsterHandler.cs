﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class maxNCurrent{
	public maxNCurrent(int m,int c){
		current = c;
		max = m;
	}
	public int getCurrent () {
		return current;
	}
	public void setCurrent(int c){
		current = c;
	}

	public void currentAdd () {
		current++;
	}

	public void currentMinus () {
		current--;
	}
	public void setMax(int m){
		max = m;
	}
	public int getMax(){
		return max;
	}
	int current;
	int max;
};

public class MonsterHandler : MonoBehaviour {
	
	Dictionary<string,maxNCurrent> monsterMax = new Dictionary<string,maxNCurrent> ();
	public Transform monster;
	List<Creature> monsters = new List <Creature> ();
	private Dictionary<string , List<Rect>> monsterGenerateArea = new Dictionary<string, List<Rect>>();
	// Use this for initialization
	void Start () {
		//createMonster ("slime", new Vector3 (5, 0, 0));
		//createMonster ("astolfo", new Vector3 (-5, 0, 0));
		monsterMax.Add ("slime_green", new maxNCurrent (100, 0));
		monsterMax.Add ("astolfo", new maxNCurrent (0, 0));

		generateAreaSetting ();
	}

	private void generateAreaSetting () {
		List<Rect> generateArea = new List<Rect> ();
		generateArea.Add (new Rect (20, 20, 30, 30));
		monsterGenerateArea.Add ("slime_green", generateArea);
		monsterGenerateArea.Add ("astolfo", generateArea);
	}
	
	// Update is called once per frame
	void Update () {
		generateMonster ();
	}

	void LateUpdate () {
		for (int i = 0; i < monsters.Count; i++) {
			if (monsters [i].getHp <= 0) {
				GetComponent<DataLib> ().missionCtrl.killEvent (monsters [i].getName());
				dropItem ((Mob)monsters [i]);
				Destroy (monsters [i].gameObject);
				monsterMax [monsters [i].getName ().ToLower()].currentMinus ();
				monsters.RemoveAt (i);
			}
		}
	}

	void createMonster (string name, Vector3 position) {
		monsters.Add (Instantiate (monster.Find (name), position, Quaternion.identity).gameObject.GetComponent<Creature> ());
	}

	void generateMonster(){
		List<string> keys = new List<string> (monsterMax.Keys);
		foreach(string key in keys) {
			maxNCurrent temp = monsterMax [key];
			if (Random.Range (0, 500) < temp.getMax () - temp.getCurrent () && temp.getCurrent () < temp.getMax ()) {
				Vector3 position = randomPosition(monsterGenerateArea [key] [Random.Range (0, monsterGenerateArea [key].Count)]);
				createMonster (key, position);
				monsterMax [key].currentAdd ();
			}
		}

	}

	private Vector3 randomPosition (Rect area) {
		return new Vector3 (Random.Range (area.xMin, area.xMax), Random.Range (area.yMin, area.yMax), 0);
	}

	public void dropItem (Mob mob) {
		dropRandomItem (mob.getDrops ().item);
		GetComponent<DataLib> ().addMoney (mob.getDrops ().money);
		GetComponent<DataLib> ().addExp (mob.getDrops ().exp);
	}

	private void dropRandomItem (Dictionary<Item, int> items) {
		if (items == null)
			return;
		foreach (KeyValuePair<Item, int> item in items)
			if (Random.Range(0, 99) < item.Value)
				GetComponent<DataLib> ().inventory.addItem (item.Key);
	}
}
