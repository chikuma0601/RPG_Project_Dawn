﻿using System;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * This reads the tmx/xml files generated from tiles.
 *
 */
public class MapReader : MonoBehaviour {
	public GameObject sampleGameObject;
	private Sprite[] tile;

	private bool addLayer = false;
	private bool addCollider = false;
	private string filePath = "Map/forest_02";
	private XmlDocument map = new XmlDocument();

	private const float tileSize = 0.8f;
	private const float tilePixel = 0.16f;

	private float mapWidth;
	private float mapHeight;
	private Vector3 startAnchor;
	
	void Start () {
		tile = Resources.LoadAll<Sprite> ("MapSprite/base");
		map.LoadXml (((TextAsset)Resources.Load (filePath)).text);

		initMap ();
	}

	private void Instantiate (Vector3 position, Sprite sprite) {
		GameObject mapBox = (GameObject)Instantiate (sampleGameObject, position, Quaternion.identity, transform);
		mapBox.GetComponent<SpriteRenderer> ().sprite = sprite;
		mapBox.transform.localScale = new Vector3 (5, 5, 1);

		if (addCollider)
			addMapCollider (mapBox);
		if (addLayer)
			addMapLayer (mapBox);
	}

	private void initMap () {
		mapWidth = Convert.ToInt32 (map.SelectSingleNode ("map").Attributes ["width"].Value);
		mapHeight = Convert.ToInt32 (map.SelectSingleNode ("map").Attributes ["height"].Value);
		startAnchor = new Vector3 (-mapWidth / 2f * tileSize, mapHeight / 2f * tileSize, 1);

		paintMap ();
	}

	private void paintMap () {
		XmlNodeList layers = map.SelectNodes ("./map/layer");
		for (int i = 0; i < layers.Count; ++i) {
			string datas = layers [i].SelectSingleNode ("./data").InnerText;
			string[] separators = { ",", "\n", "\r" };
			string[] data = datas.Split (separators, StringSplitOptions.RemoveEmptyEntries);

			extraHandle (layers [i].Attributes ["name"].Value);

			for (int j = 0; j < data.Length; ++j) {
				if (data [j] != "0") {
					int num = Convert.ToInt32 (data [j]) - 1;
					Vector3 position = startAnchor + Vector3.right * (j % mapWidth) * tileSize + Vector3.down * (j / mapHeight) * tileSize;
					Instantiate (position, tile [num]);
				}
			}
		}
	}
		
	private void extraHandle (string layerName) {
		if (layerName == "collider") {
			addCollider = true;
			addLayer = false;
		} else if (layerName == "above") {
			addLayer = true;
			addCollider = false;
		} else {
			addCollider = false;
			addLayer = false;
		}
	}

	private void addMapCollider (GameObject mapBox) {
		int i = 12;
		List<Vector2> center = new List<Vector2> {
			new Vector2 (-0.06f,  0.06f), new Vector2 (-0.02f, 0.06f), new Vector2 (0.02f,  0.06f), new Vector2 (0.06f,  0.06f), 
			new Vector2 (-0.06f,  0.02f), 															new Vector2 (0.06f,  0.02f), 
			new Vector2 (-0.06f, -0.02f), 															new Vector2 (0.06f, -0.02f), 
			new Vector2 (-0.06f, -0.06f), new Vector2 (-0.02f,-0.06f), new Vector2 (0.02f, -0.06f), new Vector2 (0.06f, -0.06f), 
		};
		while (i-- > 0) {
			CircleCollider2D collider = mapBox.AddComponent<CircleCollider2D> ();
			collider.offset = center [i];
			collider.radius = (tilePixel / 8f + 0.001f);
		}
	}

	private void addMapLayer (GameObject mapBox) {
		mapBox.GetComponent<SpriteRenderer> ().sortingLayerName = "object";
		mapBox.GetComponent<SpriteRenderer> ().sortingOrder = -Mathf.RoundToInt (mapBox.transform.position.y * 10f);

	}

	public Vector3 getStartAnchor () {
		return startAnchor;
	}

	public float getMapSize () {
		return Mathf.Sqrt (Mathf.Pow (mapWidth * tileSize, 2) + Mathf.Pow (mapHeight * tileSize, 2));
	}

}
