﻿using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;

/**
 * Conversation
 * Does...IDK either
 * Disposed
 */
public class Conversation {
	public List<Dialogue> talk = new List<Dialogue> ();
	public int dialogueIndex = 0;

	public Conversation(string filePath) {
		StreamReader reader = new StreamReader(new FileStream (filePath, FileMode.Open), Encoding.UTF8);
		while (!reader.EndOfStream) {
			string[] s = Regex.Split (reader.ReadLine (), ": ");
			talk.Add (new Dialogue (s [0], s [1]));
		}
	}

	public Dialogue getDialogue () {
		if (dialogueIndex >= talk.Count) {
			dialogueIndex = 0;
			return null;
		} else {
			return talk [dialogueIndex++];
		}
	}

	public override string ToString () {
		string s = "";
		foreach (Dialogue dial in talk)
			s += dial.toString () + "\n";
		return s;
	}

	public void resetDialogueIndex () {
		dialogueIndex = 0;
	}


}

