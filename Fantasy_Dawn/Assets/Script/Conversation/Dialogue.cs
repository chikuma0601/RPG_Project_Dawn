﻿using UnityEngine;
using System.Collections;

public class Dialogue : Event{

	string name;
	string sentence;

	public Dialogue(string name, string sentence) {
		this.name = name;
		this.sentence = sentence;
	}

	public string getName {
		get{
			return name;
		}
	}

	public string getSentence {
		get{
			return sentence;
		}
	}
		
	public override string ToString () {
		return toString ();
	}

	public string toString () {
		return name + ": " + sentence;
	}
}

