﻿using System;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

/********************************************************************\
 * 																	*
 *						 ConversationReader							*
 * Reads conversation from xml file and forms a conversation tree.	*
 * 																	*
\********************************************************************/


public class ConversationReader {
	private XmlDocument conversationFile = new XmlDocument();
	MissionReader mr;
	ItemReader ir;

	private string filePath = "../../../Assets/Resources/Text/slimeTask.xml";		// default path (for unit test)

	public ConversationReader () {
		mr = new MissionReader ();
	}
	public ConversationReader (string filePath, string taskPath) {							// for unity
		this.filePath = filePath;
		mr = new MissionReader (taskPath);
	}


	public ConversationNode read (int conversationCode) {
		try {
			conversationFile.LoadXml (filePath);
		}
		catch (XmlException e) {
			conversationFile.Load (filePath);
		}
		Console.WriteLine (conversationFile.InnerText);

		XmlNode xmlTop = conversationFile.SelectSingleNode ("conversation/conversationCode[attribute::code=" + conversationCode + "]");
		ConversationNode treeTop = loadDialogue (xmlTop);

		readShop(xmlTop, treeTop);
		readMission(xmlTop, treeTop);

		addOption (treeTop, xmlTop);
		setMissionContraint (treeTop, xmlTop);

		return new ConversationNode (treeTop);
	}

	private ConversationNode loadDialogue (XmlNode start) {
		List<Dialogue> conversation = new List<Dialogue> ();
		foreach (XmlNode dialogue in start.SelectNodes ("./dialogue"))
			conversation.Add (new Dialogue (dialogue.Attributes ["name"].Value, dialogue.InnerText));
		return new ConversationNode(conversation);
	}
		
	private void addOption (ConversationNode current, XmlNode start) {
		foreach (XmlNode option in start.SelectNodes("option")) {
			if (option != null) {
				ConversationNode optionNode = loadDialogue (option);

				readMission(option, optionNode);
				readShop(option, optionNode);

				setMissionContraint (current, start);
				current.addOption (new Option (option.Attributes ["opstr"].Value, optionNode, Convert.ToInt32(option.Attributes["nextCode"].Value)));
				addOption (optionNode, option);
			} else {
				return;
			}
		}
	}

	private void readMission (XmlNode start, ConversationNode top) {
		XmlNode mission = start.SelectSingleNode ("./mission");
		if (mission != null) {
			top.addEvent(mr.read (mission.Attributes ["code"].Value));
		}
	}

	private void readShop (XmlNode start, ConversationNode top) {
		ItemReader ir = new ItemReader (((TextAsset)Resources.Load("item")).text);
		List<Item> shopList = new List<Item> ();
		foreach (XmlNode item in start.SelectNodes("./shop/item")) {
			shopList.Add (ir.read(item.Attributes["code"].Value));
			MonoBehaviour.print (ir.read(item.Attributes["code"].Value));
		}
		top.addEvent (new Shop(shopList));
	}
		
	private void setMissionContraint (ConversationNode current, XmlNode start) {
		int code, nextCode;
		XmlNode missionComplete = start.SelectSingleNode ("./missionComplete");
		if (missionComplete != null) {
			code = Convert.ToInt32 (missionComplete.Attributes ["code"].Value);
			nextCode = Convert.ToInt32(missionComplete.Attributes ["nextCode"].Value);
			current.setMissionConstraint(code, nextCode);
		}
	}
}