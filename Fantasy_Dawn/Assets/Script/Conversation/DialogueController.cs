﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * Dialogue Controller
 * Used with conversationTrigger
 */
public class DialogueController : MonoBehaviour {
	private ConversationReader reader;
	private ConversationNode conversationTree;
	private int conversationIndex = 0;
	public Text dialogueText;
	private List<GameObject> optionButtonList = new List<GameObject> ();
	public Transform buttonSample;
	private bool conversationEnd = true;
	private NPC theGuyThatCalledMe;	//WTF this name
	private int optionNextCode = -1;
	private bool waitForButtonClick = false;


	// Use this for initialization
	void Start () {
		conversationIndex = 0;
		dialogueText.text = "";
		optionNextCode = -1;
	}
		
	public void init (int conversationCode, NPC theGuyThatCalledMe, string filePath) {
		string conversationXml = ((TextAsset)Resources.Load (filePath)).text;
		string missionXml = ((TextAsset)Resources.Load ("Task/task")).text;
		reader = new ConversationReader (conversationXml, missionXml);
		conversationIndex = 0;
		dialogueText.text = "";
		conversationEnd = true;
		this.theGuyThatCalledMe = theGuyThatCalledMe;
		conversationTree = reader.read (conversationCode);
		if (conversationTree == null)
			throw new UnmatchException ("can't find conversation code:" + conversationCode);
		if (missionComplete ())
			missionReport ();
		optionNextCode = -1;
	}

	public bool isTalking () {
		return !conversationEnd;
	}

	private Vector3 mousePositionToWorldosition (Vector3 mousePosition) {
		Vector3 position = Camera.main.ScreenToWorldPoint (mousePosition);
		position.z = 0;
		return position;
	}

	public void startTalking (Vector3 mousePosition) {
		if (theGuyThatCalledMe == null)
			return;
		Vector3 npcPosition = theGuyThatCalledMe.gameObject.transform.position;
		if ((mousePositionToWorldosition (mousePosition) - npcPosition).magnitude < 0.5) {
			conversationEnd = false;
			nextDialogue ();
		}
	}
	public void exitTalkingRange () {
		conversationIndex = 0;
		dialogueText.text = "";
		showBackground (false);
		conversationEnd = true;
		theGuyThatCalledMe = null;
		conversationTree = null;
		optionNextCode = -1;
		destroyButton ();
		GameObject.Find ("Canvas").transform.Find ("shop").GetComponent<GUIShop> ().openShopWindow (null);
	}

	public void nextDialogue () {
		if (!conversationEnd && !waitForButtonClick) {
			GameObject.Find ("player").GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.FreezeAll;
			if (conversationIndex == conversationTree.getDialogueList ().Count) {
				deliverEvent ();
				dialogueEnd ();
			} else {
				showBackground (true);
				getNextText ();
			}
		}
	}

	public void deliverEvent () {
		foreach (Event e in conversationTree.getEvents()) {
			if (e is Mission) {
				GameObject.Find("gameManager").GetComponent<DataLib> ().missionCtrl.addMission (e as Mission);
			} 
			else if (e is Shop) {
				if (((Shop)e).getShopList ().Count != 0) {
					print (((Shop)e).getShopList ().Count);
					GameObject.Find ("Canvas").transform.Find ("shop").GetComponent<GUIShop> ().openShopWindow (((Shop)e).getShopList ());
				}
			} else {
				print ("wrong case: " + e.GetType ().Name);
			}
		}
	}

	private void dialogueEnd () {
		if (conversationTree.optionSize () != 0) {
			waitForButtonClick = true;
			showOption ();
			conversationIndex = 0;
		} else {
			noOptionConversationHandle ();
		}
		if (optionNextCode != -1)
			theGuyThatCalledMe.changeConversation (optionNextCode);
	}

	private void noOptionConversationHandle () {
		conversationEnd = true;
		conversationIndex = 0;
		showBackground (false);
		dialogueText.text = "";
		GameObject.Find ("player").GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.FreezeRotation;
	}

	private bool missionComplete () {
		MissionConstraint constraint = conversationTree.getMissionConstraint ();
		if (constraint != null)
			constraint.ifRequireAchieve ();
		return constraint != null && constraint.getConstrainStatus ();
	}

	private void missionReport () {
		MissionController m = GameObject.Find ("gameManager").GetComponent<DataLib> ().missionCtrl;
		int missionCode = conversationTree.getMissionConstraint ().getCode ();
		m.getMissionReward (missionCode);
		m.removeMission (missionCode);
		theGuyThatCalledMe.changeConversation (conversationTree.getMissionConstraint ().getNextCode ());
		conversationTree = reader.read (conversationTree.getMissionConstraint ().getNextCode ());
	}

	public void showOption () {
		for (int i = 0, anchor = 25 * conversationTree.optionSize(); i < conversationTree.optionSize (); ++i, anchor -= 50) {
			buttonInit (i, anchor);
		}
	}

	public void buttonInit (int i, int anchor) {
		optionButtonList.Add (Instantiate (buttonSample.gameObject));
		optionButtonList[i].transform.SetParent (GameObject.Find ("Canvas").transform);
		optionButtonList[i].transform.localPosition = new Vector3(0, anchor,  0);
		optionButtonList[i].GetComponentInChildren<Text> ().text = conversationTree.getOption (i).getOptionString ();
		optionButtonList[i].GetComponent<Button> ().onClick.AddListener (createButtonListener(i));
	}

	public UnityEngine.Events.UnityAction createButtonListener (int i) {
		return () => {
			waitForButtonClick = false;
			optionNextCode = conversationTree.getOption(i).getNextConversationCode();
			conversationTree = conversationTree.nextNode (i);
			destroyButton();
			if (conversationTree != null)
				getNextText();
			conversationEnd = false;
		};
	}

	public void destroyButton () {
		foreach (GameObject b in optionButtonList)
			Destroy(b);
		optionButtonList.Clear ();
	}

	public void getNextText () {
		dialogueText.text = conversationTree.getDialogueList () [conversationIndex++].toString ();
	}

	public void showBackground (bool b) {
		GameObject.Find("Canvas").transform.Find("dialogue").transform.Find("backgroundSwitch").gameObject.SetActive (b);
	}

}
