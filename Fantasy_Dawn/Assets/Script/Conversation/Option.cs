﻿using System;

public class Option {
	
	private string optionStr;
	private int nextCode;
	private ConversationNode node;

	public Option (string optionStr, ConversationNode node, int nextCode) {
		this.optionStr = optionStr;
		this.node = node;
		this.nextCode = nextCode;
	}

	public Option (Option old) {
		this.optionStr = old.optionStr;
		this.nextCode = old.nextCode;
		if (old.node != null)
			this.node = new ConversationNode (old.node);
		else
			this.node = null;
	}

	public string getOptionString () {
		return optionStr;
	}

	public ConversationNode next () {
		return node;
	}

	public int getNextConversationCode () {
		return nextCode;
	}
}