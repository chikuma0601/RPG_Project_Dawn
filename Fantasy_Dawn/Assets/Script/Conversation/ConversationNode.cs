﻿using System;
using System.Collections.Generic;
/**
 * ConverstionTree
 * Stores all dialogues and options
 */
public class ConversationNode {
	private List<Dialogue> paragraph = new List<Dialogue> ();
	private List<Option> option = new List<Option> ();
	private List<Event> events = new List<Event>();
	private MissionConstraint constraint = null;

	public ConversationNode (List<Dialogue> paragraph) {
		this.paragraph = paragraph;
	}

	public ConversationNode(ConversationNode old) {
		this.paragraph = new List<Dialogue>(old.paragraph);
		this.option = new List<Option> (old.option);
		this.events = new List<Event> (old.events);
		this.constraint = old.constraint;
	}

	public void addEvent (Event e) {
		if (e != null)
			events.Add (e);
	}

	public List<Dialogue> getDialogueList () {
		return paragraph;
	}

	public int optionSize () {
		return option.Count;
	}

	public Option getOption (int index) {
		return option [index];
	}

	public ConversationNode nextNode (int index) {
		return option [index].next ();
	}

	public void addOption (Option op) {
		option.Add (op);
	}

	public Event getEvent (int i) {
		return events [i];
	}

	public List<Event> getEvents () {
		return events;
	}

	public int getEventSize () {
		return events.Count;
	}

	public void setMissionConstraint (int code, int next) {
		constraint = new MissionConstraint (code, next);
	}

	public MissionConstraint getMissionConstraint () { // D:
		return constraint;
	}
		
	public override string ToString () {
		string s = ""; 

		foreach (Dialogue dial in paragraph)
			s += dial + "\n";
		
		foreach (Option op in option)
			s += "option: " + op.getOptionString () + "\t";
		s += "\n";

		if (events == null) {
			s += "null event";
		} else {
			foreach (Event e in events) {
				if (e is Mission) {
					s += "MissionEvent: " + ((Mission)e).getMissionCode () + " " + ((Mission)e).getMissionName ();
				} else {
					s += "unknown event: " + e.GetType ().Name;
				}
			}
		}
		return s;
	}

	public string toString () {
		return ToString ();
	}
}