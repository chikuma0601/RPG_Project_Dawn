﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataLib : MonoBehaviour {
	public MissionController missionCtrl = new MissionController ();
	public Config config;
	public Backpack inventory = new Backpack ();
	public string itemXml; 
	private int exp = 0;
	private int money = 0;
	private int level = 1;

	void Start () {
		config = new Config ();
		itemXml = ((TextAsset)Resources.Load ("item")).text;

		inventory.addItem (new Weapon (5, "冏冏丸", 65535, "傳說中的勇者使用的武器, 使用前請高喊\"Explosion!!\"", 10000000));
		inventory.addItem (new Armor (105, "國王的新衣", 65535, "裸體最強!!!", 0));
	}

	public void addMoney(int m) {
		money += m;
	}

	public void addExp (int e) {
		exp += e;
		levelUp ();
	}

	public void levelUp () {
		while (exp >= neededExp ()) {
			exp -= neededExp ();
			level++;
			GameObject.Find ("player").GetComponent<PlayerController> ().levelUp ();
			print ("lv up ! " + level + " " + exp);
		}
	}

	public int getMoney () {
		return money;
	}

	public int getExp () {
		return exp;
	}

	public int getLv () {
		return level;
	}

	private int neededExp () {
		return (int)(1000 * System.Math.Pow(1.166, level));
	}

	public List<Mission> getMissionList () {
		return missionCtrl.getMissionList ();
	}
}
