﻿using System;
using System.Xml;
using UnityEngine;


public class Config {
	private XmlDocument configFile = new XmlDocument ();

	private KeySetting keycontrol = new KeySetting ();

	public Config () {
		configFile.LoadXml (((TextAsset)Resources.Load ("config")).text);
		readConfig ();
	}

	public void reload () {
		readConfig ();
	}

	private void readConfig () {
		readKeySetting ();
	}

	private void readKeySetting () {
		XmlNode keySettingNode = configFile.SelectSingleNode("config/keySetting");

		foreach (XmlNode move in keySettingNode.SelectSingleNode("./move").ChildNodes) {
			KeyCode key = (KeyCode)Enum.Parse (typeof(KeyCode), move.Attributes ["keyCode"].Value, true);
			keycontrol.move [move.Name] = key;
		}

		foreach (XmlNode skill in keySettingNode.SelectSingleNode("./attack").ChildNodes) {
			KeyCode key = (KeyCode)Enum.Parse (typeof(KeyCode), skill.Attributes ["keyCode"].Value, true);
			keycontrol.skill.Add (skill.Attributes ["skillName"].Value, key);
		}

		foreach (XmlNode shortCut in keySettingNode.SelectSingleNode("./shortCut").ChildNodes) {
			KeyCode key = (KeyCode)Enum.Parse (typeof(KeyCode), shortCut.Attributes ["keyCode"].Value, true);
			keycontrol.shortCut.Add (shortCut.Name, key);
		}
	} 

	public KeySetting getKeySetting () {
		return new KeySetting(keycontrol);
	}
}