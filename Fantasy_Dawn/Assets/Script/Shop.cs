﻿using System;
using System.Collections.Generic;

public class Shop : Event {
	private List<Item> itemList;

	public Shop (List<Item> itemList){
		this.itemList = itemList;
	}

	public List<Item> getShopList () {
		return itemList;
	}
}

