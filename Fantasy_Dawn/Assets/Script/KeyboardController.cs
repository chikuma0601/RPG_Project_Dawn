﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class KeyboardController : MonoBehaviour {
	public GameObject player;
	public GameObject debugTerminal;
	private KeySetting keycontrol; 
	//private bool inDebugMode = false;
	// Use this for initialization
	void Start () {
		keycontrol = GetComponent<DataLib> ().config.getKeySetting ();
		//attack = playerAttackCtrl ();
	}
	
	// Update is called once per frame
	void Update () {
		shortCut ();
		playerCtrl ();
		//debug_shortCut ();
		openDebugTerminal();
		//showInventory ();	//maybe move to debug
	}

	void OnGUI () {
		scroll ();
	}

	public void scroll () {
		GameObject.Find ("Canvas").transform.Find ("inventory").GetComponent<GUIInventory> ().scrollByMouseWheel(Input.mouseScrollDelta.y);
		GameObject.Find ("Canvas").transform.Find ("shop").GetComponent<GUIShop> ().scrollByMouseWheel(Input.mouseScrollDelta.y);
		GameObject.Find ("Canvas").transform.Find ("mission").GetComponent<GUIMission> ().scrollByMouseWheel(Input.mouseScrollDelta.y);
	}

	public void playerCtrl () {
		playerMovingCtrl ();
		IEnumerator attack = playerAttackCtrl ();
		if(attack != null)
			StartCoroutine (attack);
	}

	public void shortCut () {
		foreach (KeyValuePair<string, KeyCode> key in keycontrol.shortCut) {
			if (Input.GetKeyDown (key.Value)) {
				switch (key.Key) {
				case "missionList":
					//GetComponent<DataLib> ().missionCtrl.showMissionList ();
					GameObject.Find ("Canvas").transform.Find ("mission").GetComponent<GUIMission> ().openMissionWindow();
					break;
				case "talking":
					if (!GetComponent<DialogueController> ().isTalking ()) {
						GetComponent<DialogueController> ().startTalking (Input.mousePosition);
					}
					else {
						GetComponent<DialogueController> ().nextDialogue ();
					}
					break;
				case "backpack":
					GetComponent<DataLib> ().inventory.showInventory ();
					GameObject.Find ("Canvas").transform.Find ("inventory").GetComponent<GUIInventory> ().openInventory();
					break;
				case "sortBackpack" :
					GetComponent<DataLib> ().inventory.sortbyId ();
					break;
				case "characterDetail":
					GameObject.Find ("Canvas").transform.Find ("characterStatus").GetComponent<GUIStatus> ().showStatusWindow ();
					break;
				default:
					break;
				}
			}
		}
	}

	public void openDebugTerminal () {	//in dev
		if (Input.GetKeyDown ("`")) {
			//inDebugMode = tr;
			debugTerminal.SetActive (true);
			debugTerminal.GetComponent<terminal> ().openTerminal ();
		}
	}

	/*
	public void debug_shortCut () {
		if (Input.GetKeyDown ("`")) {
			inDebugMode = !inDebugMode;
			debugTerminal.SetActive (inDebugMode);
		}
		if (inDebugMode) {
			if (Input.GetKeyDown ("q")) {
				player.GetComponent<PlayerController> ().gainBuff (new Recover (65535));
				player.GetComponent<PlayerController> ().gainBuff (new Atk (86400, 65536));
			}

			if (Input.GetKeyDown ("v")) {
				List<Item> commodity = new List<Item> ();
				commodity.Add (new Material (200, "起始之鎮", "主角誕生的城鎮", 2147483647));
				commodity.Add (new Weapon (5, "冏冏丸", 65535, "傳說中的勇者使用的武器, 使用前請高喊\"Explosion!!\"", 10000000));
				commodity.Add (new Armor (105, "國王的新衣", 65535, "裸體最強!!!", 0));
				commodity.Add (new Weapon (5, "冏冏丸", 65535, "傳說中的勇者使用的武器, 使用前請高喊\"Explosion!!\"", 10000000));
				commodity.Add (new Armor (105, "國王的新衣", 65535, "裸體最強!!!", 0));
				commodity.Add (new Weapon (5, "冏冏丸", 65535, "傳說中的勇者使用的武器, 使用前請高喊\"Explosion!!\"", 10000000));
				commodity.Add (new Armor (105, "國王的新衣", 65535, "裸體最強!!!", 0));
				commodity.Add (new Weapon (5, "冏冏丸", 65535, "傳說中的勇者使用的武器, 使用前請高喊\"Explosion!!\"", 10000000));
				commodity.Add (new Armor (105, "國王的新衣", 65535, "裸體最強!!!", 0));
				commodity.Add (new Weapon (5, "冏冏丸", 65535, "傳說中的勇者使用的武器, 使用前請高喊\"Explosion!!\"", 10000000));
				commodity.Add (new Armor (105, "國王的新衣", 65535, "裸體最強!!!", 0));
				commodity.Add (new Material (300, "虛無", "", 2147483647));
				GameObject.Find ("Canvas").transform.Find ("shop").GetComponent<GUIShop> ().openShopWindow (commodity);
			}
		}
	}*/

	public void showDetail () {
		string s;
		s = "name :" + GameObject.Find ("player").GetComponent<PlayerController> ().getName () + "\n";
		s += "Lv: " + GetComponent<DataLib> ().getLv () + ", exp: " + GetComponent<DataLib> ().getExp () + "\n";
		s += GameObject.Find ("player").GetComponent<PlayerController> ().status.toString ();
		print (s);

	}
		
	private bool isMove = false;
	public void playerMovingCtrl () {
		isMove = false;
		if (Input.GetKey (keycontrol.move["up"]) && !Input.GetKey (keycontrol.move["down"]) && !isMove) {
			player.GetComponent<PlayerController> ().move ("up");
			isMove = true;
		}
		if (Input.GetKey (keycontrol.move["down"]) && !Input.GetKey (keycontrol.move["up"]) && !isMove) {
			player.GetComponent<PlayerController> ().move ("down");
			isMove = true;
		}
		if (Input.GetKey (keycontrol.move["left"]) && !Input.GetKey (keycontrol.move["right"]) && !isMove) {
			player.GetComponent<PlayerController> ().move ("left");
			isMove = true;
		}
		if (Input.GetKey (keycontrol.move["right"]) && !Input.GetKey (keycontrol.move["left"]) && !isMove) {
			player.GetComponent<PlayerController> ().move ("right");
			isMove = true;
		}
		if (!isMove)
			player.GetComponent<PlayerController> ().move ("stop");
		/*
		if (!(Input.GetKey (keycontrol.move["up"]) || Input.GetKey (keycontrol.move["down"]) || Input.GetKey (keycontrol.move["left"]) || Input.GetKey (keycontrol.move["right"]))) {
			player.GetComponent<PlayerController> ().move ("stop");
		}
		*/
	}

	public IEnumerator playerAttackCtrl () {
		foreach (KeyValuePair<string, KeyCode> key in keycontrol.skill) {
			if (Input.GetKeyDown (key.Value)) {
				return player.GetComponent<PlayerController> ().attack (key.Key);
			}
		}
		return null;
	}

	public void closeTerminal () {
		debugTerminal.SetActive(false);
	}
		
}
