﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class terminal : MonoBehaviour {
	
	[SerializeField]
	private InputField inputArea;

	void Start () {
		//inputArea = gameObject.GetComponent<InputField> ();
		inputArea.onEndEdit.AddListener (delegate {commandHandler();});
	}

	public void openTerminal () {
		inputArea.enabled = true;
		GameObject.Find ("gameManager").GetComponent<KeyboardController> ().enabled = false;
	}

	public void commandHandler () {
		string command = inputArea.text;
		inputArea.text = "";

		searchCommand (command);
	}

	private void searchCommand (string s) {
		string com = splitCommandBySpace (s);
		print (com);
		switch (com) {
		case "ping":
			inputArea.text = "pong!";
			print ("pong!");
			break;
		case "lee":
		case "danny":
			inputArea.text = "GO TO WORK !";
			break;
		case "super":
			print ("Issued command : super");
			GameObject.Find("player").GetComponent<PlayerController> ().gainBuff (new Recover (65535));
			GameObject.Find("player").GetComponent<PlayerController> ().gainBuff (new Atk (86400, 65536));
			break;
		case "shop":
			openShop ();
			break;
		case "additem":
			inputArea.text = "No.";
			break;
		case "quit":
			GameObject.Find ("gameManager").GetComponent<KeyboardController> ().enabled = true;
			inputArea.enabled = false;
			GameObject.Find ("gameManager").GetComponent<KeyboardController> ().closeTerminal ();
			break;
		}
	}

	private void openShop () {
		List<Item> commodity = new List<Item> ();
		commodity.Add (new Material (200, "起始之鎮", "主角誕生的城鎮", 2147483647));
		commodity.Add (new Weapon (5, "冏冏丸", 65535, "傳說中的勇者使用的武器, 使用前請高喊\"Explosion!!\"", 10000000));
		commodity.Add (new Armor (105, "國王的新衣", 65535, "裸體最強!!!", 0));
		commodity.Add (new Weapon (5, "冏冏丸", 65535, "傳說中的勇者使用的武器, 使用前請高喊\"Explosion!!\"", 10000000));
		commodity.Add (new Armor (105, "國王的新衣", 65535, "裸體最強!!!", 0));
		commodity.Add (new Weapon (5, "冏冏丸", 65535, "傳說中的勇者使用的武器, 使用前請高喊\"Explosion!!\"", 10000000));
		commodity.Add (new Armor (105, "國王的新衣", 65535, "裸體最強!!!", 0));
		commodity.Add (new Weapon (5, "冏冏丸", 65535, "傳說中的勇者使用的武器, 使用前請高喊\"Explosion!!\"", 10000000));
		commodity.Add (new Armor (105, "國王的新衣", 65535, "裸體最強!!!", 0));
		commodity.Add (new Weapon (5, "冏冏丸", 65535, "傳說中的勇者使用的武器, 使用前請高喊\"Explosion!!\"", 10000000));
		commodity.Add (new Armor (105, "國王的新衣", 65535, "裸體最強!!!", 0));
		commodity.Add (new Material (300, "虛無", "", 2147483647));
		GameObject.Find ("Canvas").transform.Find ("shop").GetComponent<GUIShop> ().openShopWindow (commodity);
	}

	private string splitCommandBySpace (string s) {
		string com = "";
		foreach (char a in s) {
			if (a == ' ')
				break;
			com += a;
		}
		return com;
	}
}
