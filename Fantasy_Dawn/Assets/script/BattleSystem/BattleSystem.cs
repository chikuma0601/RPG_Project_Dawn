﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleSystem : MonoBehaviour {

	public static void damageCalculator (float damage, Creature defender) {
		defender.getHp -= Mathf.CeilToInt(damage * (100f / (100f + defender.getDef)));
	}
}