﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackRange : MonoBehaviour {
	private string hostile;
	//public string extraSkillEffect { get; set; }

	public Skill skill = null;

	private void OnTriggerEnter2D (Collider2D collide) {
		hostile = transform.GetComponentInParent<Creature> ().getHostile();

		if (collide.CompareTag (hostile + "_hitBox")) {
			//print ("attack");
			Creature attacker = transform.GetComponentInParent<Creature> ();
			Creature defender = collide.transform.parent.GetComponent<Creature> ();

			BattleSystem.damageCalculator (attacker.getAtk * skill.skillAttackRate, defender);

			foreach (Buff buff in skill.buffSelf)
				attacker.gainBuff (buff);

			foreach (Buff buff in skill.buffHostile)
				defender.gainBuff (buff);
		}
	}
}